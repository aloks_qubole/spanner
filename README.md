# spanner

[![Coverage Status](https://coveralls.io/repos/github/alokic/spanner/badge.svg?t=4uMFW5)](https://coveralls.io/github/alokic/spanner)
[![Build Status](https://drone.alokic.com/api/badges/alokic/spanner/status.svg)](https://drone.alokic.com/alokic/spanner)

Apigateway and RBAC for admin apis and resources

## Technical Specs


## Health Check

Standard health check end point: `/health`

## Local Setup

### Pre-requisites

```
# install go
$ brew install go@1.12

# install dep
$ brew install dep

# Install postgreSQL
Skip this step if you already have `postgresql` installed
$ brew install mysql
```

### Build

1. Make sure GOROOT and GOPATH are properly set.
2. Set PATH

    ```export PATH=$PATH:$GOPATH/bin```

3. Git clone the repo in $GOPATH/src/github.com/alokic.

    ```cd $GOPATH/src/github.com/alokic```

    ```git clone git@github.com:alokic/spanner.git```

    ```cd spanner```

4. Check all deps are there by running below

   ```$dep ensure```
  
5. Setup mysql

   ```brew install mysql```

   ```brew services start mysql``` 

6. Create spanner database if not already, from psql:   

   ```mysql```

   ```> CREATE DATABASE spanner;``` <- psql cmd line

7. Run db migrations:

   ```./scripts/migrate.sh $SPANNER_DB_URL up```

8. Export required environment variables

    ```export SPANNER_ENV=<blah>```

    ```export SPANNER_NEWRELIC_LICENSE=<blah>```

    ```export SPANNER_REDIS_URL=<blah>```

    ```export SPANNER_DB_URL="mysql://<username>:<password>@<server>/spanner"```

    ```export SPANNER_DB_TEST_URL="mysql://<username>:<password>@<server>/spanner_test"```

9. Build the app

   ```make build```

   ```'spanner' gets installed in $GOPATH/bin```

11. Run the app

   ```spanner --help```

12. Run the app

   ```spanner --env <env_name>```

   ```default env is development```

### tests

1. Create test database in postgres via psql

    ```psql```

   ```> CREATE DATABASE spanner_test;```

2. Export required environment variables

    ```export SPANNER_DB_TEST_URL="mysql://<username>:<password>@localhost:5432/spanner_test?sslmode=disable"```
    ```export spanner_REDIS_TEST_URL="redis://localhost:6379/3"```

3. Run migrations

```./scripts/migrate.sh $SPANNER_DB_TEST_URL up```

4. Run all tests now

  ```make test```

## Release Process

We follow [Feature branch workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow)

Feature branches are merged in `master` branch after `PR` approval.

Deployment is triggered when we tag `master` branch.

Tagging convention for `production` environment is `v.MAJOR_NUMBER.MINOR_NUMBER.PATCH_NUMBER`.

We increment `NUMBERS` when:

* `MAJOR_NUMBER` when breaking backward compatibility.
* `MAJOR_NUMBER` when adding a new feature which doesn’t break compatibility.
* `PATCH_NUMBER` when fixing a bug without breaking compatibility.

Tagging convention for `staging` is `v.MAJOR_NUMBER.MINOR_NUMBER.PATCH_NUMBER-<SUFFIX>.<CHILD_SUFFIX>`.

* `SUFFIX` is mandatory for `staging` deployments. Valid `SUFFIX` are `alpha`, `beta`, `rc`.
* `CHILD_SUFFIX` is optional and should be used for incremental updates in a tag like `v.1.0.0-rc.1`
