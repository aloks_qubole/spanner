package upstream

import (
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"

	"github.com/alokic/spanner/internal/filters"

	"github.com/alokic/gopkg/httputils"
	"github.com/pkg/errors"
)

// Upstream struct
// IsDefault=true creates a default proxy router for a service.
// Default proxy is called when request does not have domain information
// in header to choose the host to route the request.
type Upstream struct {
	Name        string   `json:"name"`
	Env         string   `json:"env"`
	Domain      string   `json:"domain"`
	Labels      []string `json:"labels"`
	IsDefault   bool     `json:"is_default"`
	PostFilters []string `json:"post_filters"`
	PreFilters  []string `json:"pre_filters"`
}

// router definition.
// It supports router interface
type router struct {
	name    string
	rp      *httputil.ReverseProxy
	hostURL *url.URL
	handler http.Handler
	pre     []filters.PreFunc
	post    []filters.PostFunc
}

// New is constructor for upstream.
func New(ups *Upstream, pre []filters.PreFunc, post []filters.PostFunc) (http.Handler, error) {
	u, err := url.Parse(strings.ToLower(ups.Domain))
	if err != nil {
		return nil, errors.Wrap(err, "newUpstreamRouter")
	}

	p := &router{
		rp:      httputil.NewSingleHostReverseProxy(u),
		hostURL: u,
		pre:     pre,
		post:    post,
	}

	p.rp.ModifyResponse = p.wrapPostHandler()
	p.rp.ErrorHandler = p.wrapErrorHandler()
	p.handler = p.wrapPreHandler()

	return p, nil
}

// ServeHTTP to satisfy http.Handler.
func (p *router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	p.handler.ServeHTTP(w, r)
}

// wrapPreHandler wraps reverse-proxy with pre-process filters.
func (p *router) wrapPreHandler() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		for _, fn := range p.pre {
			if status, err := fn(r); err != nil {
				httputils.WriteError(err, status, w)
				return
			}
		}

		// Update the headers to allow for SSL redirection
		r.Header.Set("X-Forwarded-Host", r.Host)
		r.URL.Host = p.hostURL.Host
		r.URL.Scheme = p.hostURL.Scheme
		r.Host = p.hostURL.Host
		p.rp.ServeHTTP(w, r)
	})
}

type respErr struct {
	err    error
	status int
}

func (e *respErr) Error() string {
	return e.err.Error()
}

// wrapPostHandler wraps response post filters
func (p *router) wrapPostHandler() func(*http.Response) error {
	return func(resp *http.Response) error {
		for _, fn := range p.post {
			if status, err := fn(resp); err != nil {
				return &respErr{err: err, status: status}
			}
		}

		return nil
	}
}

func (p *router) wrapErrorHandler() func(http.ResponseWriter, *http.Request, error) {
	return func(w http.ResponseWriter, r *http.Request, err error) {
		switch v := err.(type) {
		case *respErr:
			httputils.WriteError(v.err, v.status, w)
		default:
			httputils.WriteError(err, http.StatusBadGateway, w)
		}
	}
}
