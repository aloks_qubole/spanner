package proxy

import (
	"net/http"
	"strings"
	"sync"

	"github.com/pkg/errors"

	"github.com/alokic/gopkg/httputils"
)

var (
	// ErrNoProxyToRoute means no suitable proxy router found to route the request. DefaultProxy is also nil.
	ErrNoProxyToRoute = errors.New("no host to route the request")
	// ErrNoProxyForSvc means no proxy is registerred for service.
	ErrNoProxyForSvc = errors.New("no proxy registered for service")
)

// Proxy can hold multiple fallback routers.
type Proxy struct {
	sync.RWMutex
	routers      map[string]http.Handler
	defaultProxy http.Handler
	rules        []*Rule
}

// NewProxy is constructor for multi-service router.
func NewProxy(rules []*Rule) (*Proxy, error) {
	p := &Proxy{
		routers: make(map[string]http.Handler),
		rules:   rules,
	}

	return p, nil
}

// AddUpstream adds an upstream to a proxy.
func (m *Proxy) AddUpstream(name string, upstream http.Handler, isDefault bool) error {
	defer m.Unlock()
	m.Lock()

	p, ok := m.routers[strings.ToLower(name)]
	if ok {
		return nil
	}
	p = upstream

	m.routers[strings.ToLower(name)] = p

	if isDefault {
		m.defaultProxy = p
	}

	return nil
}

// ServeHTTP to satisfy http.Handler.
func (m *Proxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	p, err := m.getUpstream(r)
	if err != nil {
		httputils.WriteError(err, http.StatusBadGateway, w)
		return
	}
	p.ServeHTTP(w, r)
}

func (m *Proxy) getUpstream(req *http.Request) (http.Handler, error) {
	var upstream string

	for _, r := range m.rules {
		if !r.Evaluate(req) {
			continue
		}
		upstream = r.Upstream
	}

	if upstream == "" {
		if m.defaultProxy == nil {
			return nil, ErrNoProxyToRoute
		}
		return m.defaultProxy, nil
	}

	router, ok := m.routers[upstream]
	if !ok {
		return nil, ErrNoProxyForSvc
	}
	return router, nil
}
