package proxy

import (
	"net/http"
	"regexp"
	"strings"
)

// Rule is struct of routing.
type Rule struct {
	Upstream      string               `json:"upstream"`
	Headers       []*HTTPHeaderRule    `json:"http_headers"`
	HostHeaders   []*HostHeaderRule    `json:"host_headers"`
	RequestMethod []*RequestMethodRule `json:"request_method"`
	Queries       []*QueryRule         `json:"queries"`
	Paths         []*PathRule          `json:"paths"`
}

// Evaluate a rule
func (r *Rule) Evaluate(req *http.Request) bool {
	for _, h := range r.Headers {
		if !h.evaluate(req) {
			return false
		}
	}

	for _, h := range r.HostHeaders {
		if !h.evaluate(req) {
			return false
		}
	}

	for _, h := range r.RequestMethod {
		if !h.evaluate(req) {
			return false
		}
	}

	for _, h := range r.Queries {
		if !h.evaluate(req) {
			return false
		}
	}

	for _, h := range r.Paths {
		if !h.evaluate(req) {
			return false
		}
	}

	return true
}

// HTTPHeaderRule struct
type HTTPHeaderRule struct {
	Name   string   `json:"name"`
	Values []string `json:"values"`
}

// evaluate
func (h *HTTPHeaderRule) evaluate(req *http.Request) bool {
	val := req.Header.Get(h.Name)
	for _, v := range h.Values {
		if v == val {
			return true
		}
	}
	return false
}

// HostHeaderRule struct
type HostHeaderRule struct {
	Values []string `json:"values"`
}

// evaluate
func (h *HostHeaderRule) evaluate(req *http.Request) bool {
	val := strings.ToLower(req.Header.Get("Host"))
	for _, v := range h.Values {
		if strings.ToLower(v) == val {
			return true
		}
	}
	return false
}

// RequestMethodRule struct
type RequestMethodRule struct {
	Values []string `json:"values"`
}

// evaluate
func (h *RequestMethodRule) evaluate(req *http.Request) bool {
	val := strings.ToLower(req.Method)
	for _, v := range h.Values {
		if strings.ToLower(v) == val {
			return true
		}
	}
	return false
}

// QueryRule struct
type QueryRule struct {
	Name   string   `json:"name"`
	Values []string `json:"values"`
}

// evaluate
func (h *QueryRule) evaluate(req *http.Request) bool {
	val := req.URL.Query().Get(h.Name)
	for _, v := range h.Values {
		if v == val {
			return true
		}
	}
	return false
}

// PathRule struct
type PathRule struct {
	Values []string `json:"values"`
}

// evaluate
func (h *PathRule) evaluate(req *http.Request) bool {
	val := req.URL.Path
	for _, v := range h.Values {
		m, _ := regexp.MatchString(v, val)
		if m {
			return true
		}
	}
	return false
}
