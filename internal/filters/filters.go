package filters

import (
	"net/http"
)

// PostFunc process request after receiving response from upstream
type PostFunc func(*http.Response) (status int, err error)

// PreFunc process request before dispatching to endpoint
type PreFunc func(*http.Request) (status int, err error)

// PostFilter struct
type PostFilter struct {
	Fn  PostFunc
	Key string
}

// PreFilter struct
type PreFilter struct {
	Fn  PreFunc
	Key string
}

// Filters struct
type Filters struct {
	pre  map[string]*PreFilter
	post map[string]*PostFilter
}

// New is contructor
func New() *Filters {
	return &Filters{
		pre:  map[string]*PreFilter{},
		post: map[string]*PostFilter{},
	}
}

// SetPre sets a custom prefilter
func (f *Filters) SetPre(key string, fn PreFunc) {
	f.pre[key] = &PreFilter{Fn: fn, Key: key}
}

// GetPre get a prefilter
func (f *Filters) GetPre(key string) *PreFilter {
	return f.pre[key]
}

// SetPost sets a custom postfilter
func (f *Filters) SetPost(key string, fn PostFunc) {
	f.post[key] = &PostFilter{Fn: fn, Key: key}
}

// GetPost get a postfilter
func (f *Filters) GetPost(key string) *PostFilter {
	return f.post[key]
}
