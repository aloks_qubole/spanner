package post

import (
	"net/http"
	"net/url"
	"strings"

	"github.com/pkg/errors"
)

// RedirectLocalhostToHTTP redirects https redirect to http://localhost:xxxx.
// Its needed when localhost is not running on ssl.
func RedirectLocalhostToHTTP(resp *http.Response) (status int, err error) {
	l := resp.Header.Get("Location")

	if !isRedirect(resp) {
		return http.StatusOK, nil
	}

	if !strings.Contains(l, "localhost") {
		return http.StatusOK, nil
	}

	u, err := url.Parse(l)
	if err != nil {
		return http.StatusUnprocessableEntity, errors.Wrap(err, "redirectLocalhostToHTTP: failed to close body")
	}

	resp.Header.Set("Location", "http://"+u.Host+u.Path)

	return http.StatusOK, nil
}

func isRedirect(resp *http.Response) bool {
	return resp.StatusCode >= http.StatusMultipleChoices && resp.StatusCode <= http.StatusPermanentRedirect
}
