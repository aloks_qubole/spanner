package post

import (
	"bytes"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"
)

// RewriteBodyNop of response
func RewriteBodyNop(resp *http.Response) (status int, err error) {
	b, err := ioutil.ReadAll(resp.Body) //Read html
	if err != nil {
		return http.StatusUnprocessableEntity, errors.Wrap(err, "rewriteBodyNop: failed to read body")
	}

	err = resp.Body.Close()
	if err != nil {
		return http.StatusUnprocessableEntity, errors.Wrap(err, "rewriteBodyNop: failed to close body")
	}

	resp.Body = ioutil.NopCloser(bytes.NewReader(b))
	resp.ContentLength = int64(len(b))

	return http.StatusOK, nil
}
