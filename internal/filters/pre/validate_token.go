package pre

import (
	"net/http"

	"github.com/alokic/spanner/internal/filters"
	"github.com/alokic/spanner/pkg/oauth2"
	"github.com/pkg/errors"
)

// JWTer interface
type JWTer interface {
	Generate(map[string]interface{}) (string, error)
}

// ValidateToken validates the access token in a request as token
func ValidateToken(svc oauth2.Service, jwt JWTer) filters.PreFunc {
	return func(r *http.Request) (status int, err error) {
		cookie, err := r.Cookie("jwttoken")
		if err != nil {
			return http.StatusUnauthorized, errors.Wrap(err, "validate_access_token")
		}

		resp, err := svc.SignIn(r.Context(), &oauth2.SignInRequest{Token: cookie.Value})
		if err != nil {
			return http.StatusUnauthorized, errors.Wrap(err, "validate_access_token")
		}

		token, err := jwt.Generate(map[string]interface{}{"email": resp.Email})
		if err != nil {
			return http.StatusUnauthorized, errors.Wrap(err, "validate_access_token")
		}

		r.Header.Set("x-service-token", token)
		return http.StatusOK, nil
	}
}
