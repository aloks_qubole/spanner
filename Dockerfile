FROM golang:1.12-alpine as builder

RUN apk add --no-cache alpine-sdk


ENV PATH /go/bin:/usr/local/go/bin:$PATH
ENV GOPATH /go

WORKDIR /go/src/github.com/alokic/spanner
COPY . .

RUN make build

###############################

FROM alpine:latest as spanner-image

RUN apk add --no-cache bash curl

ENV GOPATH /go

COPY --from=builder /go/bin/spanner /usr/bin/spanner
COPY --from=builder ${GOPATH}/src/github.com/alokic/spanner/config/ ${GOPATH}/src/github.com/alokic/spanner/config
COPY --from=builder /etc/ssl/certs/ /etc/ssl/certs

WORKDIR /go/src/github.com/alokic/spanner

CMD [ "spanner" ]