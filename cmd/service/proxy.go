package service

import (
	"encoding/json"
	"net/http"

	"github.com/alokic/spanner/internal/filters"
	"github.com/alokic/spanner/internal/proxy"
	"github.com/alokic/spanner/internal/upstream"
	jsoniter "github.com/json-iterator/go"
	"github.com/pkg/errors"
)

// Filters interface
type Filters interface {
	GetPre(string) *filters.PreFilter
	GetPost(string) *filters.PostFilter
}

// ProxyInput struct.
type ProxyInput struct {
	RoutingRules []interface{}
	Upstreams    []interface{}
	Filters      Filters
}

// Proxy struct.
type Proxy struct {
	*ProxyInput
	Proxy *proxy.Proxy
}

// NewProxy constructor.
func NewProxy(in *ProxyInput) (*Proxy, error) {
	p := &Proxy{ProxyInput: in}

	rs, err := p.toRoutingRules(in.RoutingRules)
	if err != nil {
		return nil, errors.Wrap(err, "initProxy")
	}

	p.Proxy, err = proxy.NewProxy(rs)
	if err != nil {
		return nil, errors.Wrap(err, "initProxy")
	}

	for _, ups := range in.Upstreams {
		u, err := p.toUpstream(ups)
		if err != nil {
			return nil, errors.Wrap(err, "initProxy")
		}

		hn, err := upstream.New(u, p.preFilters(u.PreFilters), p.postFilters(u.PostFilters))
		if err != nil {
			return nil, errors.Wrap(err, "initProxy")
		}

		err = p.Proxy.AddUpstream(u.Name, hn, u.IsDefault)
		if err != nil {
			return nil, errors.Wrap(err, "initProxy")
		}
	}

	return p, nil
}

func (p *Proxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	p.Proxy.ServeHTTP(w, r)
}

func (p *Proxy) toUpstream(upst interface{}) (*upstream.Upstream, error) {
	b, err := betterMarshal(upst)
	if err != nil {
		return nil, errors.Wrap(err, "toUpstream:json.Marshal")
	}

	var u *upstream.Upstream
	err = json.Unmarshal(b, &u)
	if err != nil {
		return nil, errors.Wrap(err, "toUpstream:json.Unmarshal")
	}

	return u, nil
}

func (p *Proxy) toRoutingRules(rules []interface{}) ([]*proxy.Rule, error) {
	rs := []*proxy.Rule{}

	b, err := betterMarshal(rules)
	if err != nil {
		return nil, errors.Wrap(err, "toRoutingRules:json.Marshal")
	}

	err = jsoniter.Unmarshal(b, &rs)
	if err != nil {
		return nil, errors.Wrap(err, "toRoutingRules:json.Unmarshal")
	}

	return rs, nil
}

// takes care of ma[interface{}]interface{} marshalling too which default json encoder does not.
func betterMarshal(m interface{}) ([]byte, error) {
	return jsoniter.Marshal(m)
}

func (p *Proxy) preFilters(keys []string) []filters.PreFunc {
	fns := []filters.PreFunc{}
	for _, key := range keys {
		fns = append(fns, p.Filters.GetPre(key).Fn)
	}
	return fns
}

func (p *Proxy) postFilters(keys []string) []filters.PostFunc {
	fns := []filters.PostFunc{}
	for _, key := range keys {
		fns = append(fns, p.Filters.GetPost(key).Fn)
	}
	return fns
}
