package service

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/alokic/gopkg/httputils"
	"github.com/go-kit/kit/log"
	"github.com/heptio/workgroup"

	"github.com/alokic/spanner/pkg/oauth2"
	"github.com/bugsnag/bugsnag-go"
	newrelic "github.com/newrelic/go-agent"
)

// HTTPServerInput struct
type HTTPServerInput struct {
	Port            int
	Router          httputils.Router
	Logger          log.Logger
	Exception       bugsnag.Configuration
	APM             newrelic.Application
	NotFoundHandler http.Handler
	OauthProvider   oauth2.Service
}

type httpServer struct {
	*HTTPServerInput
}

// HTTPServer init.
func HTTPServer(g *workgroup.Group, in *HTTPServerInput) error {
	hs := &httpServer{HTTPServerInput: in}

	hs.routes()

	h := &http.Server{Addr: ":" + fmt.Sprint(in.Port), Handler: in.Router}
	g.Add(func(stop <-chan struct{}) error {
		return h.ListenAndServe()
	})

	// shutdown http
	g.Add(func(stop <-chan struct{}) error {
		sigChan := make(chan os.Signal, 1)
		signal.Notify(sigChan, os.Interrupt)

		hs := []*http.Server{h}

		select {
		case <-stop:
		case <-sigChan:
		}

		ctx := context.Background()
		for _, h := range hs {
			err := h.Shutdown(ctx)
			if err != nil {
				return err
			}
			in.Logger.Log("shutdown", "completed", "for", h.Addr)
		}
		return nil
	})

	return nil
}

func (hs *httpServer) routes() {
	hs.handle("GET", "/", http.HandlerFunc(hs.homeHandler))
	hs.handle("GET", "/login", http.HandlerFunc(hs.loginHandler))
	hs.handle("GET", "/google_oauth_callback", http.HandlerFunc(hs.callbackHandler))
	hs.Router.NotFound(hs.loggerHandler(hs.NotFoundHandler))
}

func (hs *httpServer) handle(method, path string, handler http.Handler) {
	hs.Router.Handle(method, path, hs.wrapHandlers(path, handler))
}

// loginHandler to redirect to login page
func (hs *httpServer) loginHandler(w http.ResponseWriter, r *http.Request) {
	resp, err := hs.OauthProvider.AuthCodeURL(r.Context(), &oauth2.AuthCodeURLRequest{StateInfo: map[string]string{}})
	if err != nil {
		httputils.WriteError(err, http.StatusUnauthorized, w)
		return
	}

	http.Redirect(w, r, resp.Url, http.StatusTemporaryRedirect)
}

// CallbackHandler to handle callback after user grant
func (hs *httpServer) callbackHandler(w http.ResponseWriter, r *http.Request) {
	resp, err := hs.OauthProvider.SignUp(r.Context(), &oauth2.SignUpRequest{State: r.URL.Query().Get("state"), Code: r.URL.Query().Get("code")})
	if err != nil {
		httputils.WriteError(err, http.StatusUnauthorized, w)
		return
	}
	http.SetCookie(w, &http.Cookie{Name: "jwttoken", Value: resp.Token})
	http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
}

func (hs *httpServer) recoverHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r := recover(); r != nil {
				hs.notifyException(fmt.Errorf("%v", r))
				w.WriteHeader(http.StatusInternalServerError)
				w.Write([]byte{})
			}
		}()
		h.ServeHTTP(w, r)
	})
}

func (hs *httpServer) accessControlHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, PATCH, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}

func (hs *httpServer) apmHandler(pattern string, handler http.Handler) http.Handler {
	_, hn := newrelic.WrapHandle(hs.APM, pattern, handler)
	return hn
}

func (hs *httpServer) loggerHandler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		sw := statusWriter{ResponseWriter: w}

		defer func(begin time.Time, r *http.Request) {
			hs.Logger.Log("host", r.Host, "path", r.URL.Path, "remoteAddr", r.RemoteAddr, "method", r.Method, "status", sw.status, "content-len", sw.length, "took", time.Since(begin))
		}(time.Now(), r)

		next.ServeHTTP(&sw, r)
	})
}

func (hs *httpServer) wrapHandlers(pattern string, h http.Handler) http.Handler {
	return hs.recoverHandler(hs.loggerHandler(hs.accessControlHandler(hs.apmHandler(pattern, h))))
}

func (hs *httpServer) notifyException(err error) {
	bugsnag.Notify(err, hs.Exception)
}

func (hs *httpServer) homeHandler(w http.ResponseWriter, r *http.Request) {
	var htmlIndex = `<html>
	<body>
		<a href="/login">Google Log In</a>
	</body>
	</html>`
	fmt.Fprintf(w, htmlIndex)
}

type statusWriter struct {
	http.ResponseWriter
	status int
	length int
}

func (w *statusWriter) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

func (w *statusWriter) Write(b []byte) (int, error) {
	if w.status == 0 {
		w.status = 200
	}
	n, err := w.ResponseWriter.Write(b)
	w.length += n
	return n, err
}
