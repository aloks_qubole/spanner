package service

import (
	"time"

	"github.com/alokic/gopkg/sql"
	"github.com/alokic/gopkg/stringutils"
	"github.com/go-kit/kit/log"
	"github.com/pkg/errors"
)

// MySQLInput struct.
type MySQLInput struct {
	ConnectRetryCount int
	MaxIdleConns      int
	MaxOpenConns      int
	URL               string
	Logger            log.Logger
}

// MySQL struct.
type MySQL struct {
	DB *sql.DB
	in *MySQLInput
}

// NewMySQL constructor.
func NewMySQL(in *MySQLInput) (*sql.DB, error) {
	var err error

	db := &MySQL{in: in}
	for count := 0; count < in.ConnectRetryCount; count++ {
		err = db.connect()
		if err != nil {
			in.Logger.Log("mysql", "connect", "error", err, "retry", count < in.ConnectRetryCount)
			time.Sleep(2 * time.Second)
			continue
		}
	}

	if err != nil {
		in.Logger.Log("mysql", "connect", "error", err, "exiting", "...")
		return nil, errors.Wrap(err, "NewMysqlDB")
	}

	in.Logger.Log("mysql", "connected")
	return db.DB, nil
}

func (db *MySQL) connect() error {
	d, err := sql.NewDB("mysql", db.in.URL)

	if err == nil {
		err = d.Ping()
	}

	if err != nil {
		return errors.Wrap(err, "MySQL:connect:Ping")
	}

	// this converts all struct  name to snakecase name to map to db column
	d.MapperFunc(func(s string) string {
		return stringutils.ToLowerSnakeCase(s)
	})

	d.SetMaxIdleConns(db.in.MaxIdleConns)
	d.SetMaxOpenConns(db.in.MaxOpenConns)

	db.DB = d

	return nil
}
