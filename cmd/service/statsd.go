package service

import (
	"fmt"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/metrics/statsd"
)

// StatsDInput struct.
type StatsDInput struct {
	Prefix        string
	FlushInterval int
	Host          string
	Port          int
	Logger        log.Logger
}

// StatsD struct.
type StatsD struct {
	Client *statsd.Statsd
	in     *StatsDInput
}

// NewStatsD constructor.
func NewStatsD(in *StatsDInput) *StatsD {
	s := &StatsD{Client: statsd.New(in.Prefix, log.With(in.Logger, "ctx", "statsd")), in: in}
	go s.Client.SendLoop(time.NewTicker(time.Duration(s.in.FlushInterval)*time.Second).C, "udp", fmt.Sprintf("%s:%d", s.in.Host, s.in.Port))
	return s
}
