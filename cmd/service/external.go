package service

import (
	"fmt"

	"github.com/bugsnag/bugsnag-go"
	"github.com/go-kit/kit/log"
	newrelic "github.com/newrelic/go-agent"
)

// NewrelicInput struct.
type NewrelicInput struct {
	Server  string
	Env     string
	License string
}

// NewNewrelic constructor.
func NewNewrelic(in *NewrelicInput) (newrelic.Application, error) {
	return newrelic.NewApplication(newrelic.Config{
		AppName: fmt.Sprintf("%s-%s", in.Server, in.Env),
		License: in.License,
	},
	)
}

type printfLogger struct {
	logger log.Logger
}

func (p *printfLogger) Printf(format string, v ...interface{}) {
	p.logger.Log(fmt.Sprintf(format, v...), "")
}

// BugsnagInput struct.
type BugsnagInput struct {
	Pkg    string
	Env    string
	Key    string
	Logger log.Logger
}

// NewBugsnag constructor..
func NewBugsnag(in *BugsnagInput) bugsnag.Configuration {
	return bugsnag.Configuration{
		APIKey:          in.Key,
		ProjectPackages: []string{"service", in.Pkg},
		ReleaseStage:    in.Env,
		Logger:          &printfLogger{logger: log.With(in.Logger, "type", "bugsnag")},
	}
}
