package service

import (
	gredis "github.com/alokic/gopkg/redis"
	"github.com/go-kit/kit/log"
	"github.com/gomodule/redigo/redis"
	"github.com/pkg/errors"
)

// RedisInput struct.
type RedisInput struct {
	URL    string
	Logger log.Logger
}

// NewRedis constructor.
func NewRedis(in *RedisInput) (*redis.Pool, error) {
	r := gredis.NewRedisPool(in.URL)

	conn := r.Get()
	defer conn.Close()

	_, err := conn.Do("PING")

	if err != nil {
		in.Logger.Log("redis", "connect", "error", err, "exiting", "...")
		return nil, errors.Wrap(err, "NewRedis:Ping")
	}

	in.Logger.Log("redis", "connected")
	return r, nil
}
