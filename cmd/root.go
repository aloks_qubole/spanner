// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// BuildVars needed for commands
type BuildVars struct {
	GitCommit string
	GitBranch string
	Version   string
	RootPath  string
}

// Build vars
var (
	// GITCOMMIT means git commit tag
	GITCOMMIT = ""
	// GITBRANCH means git branch for the build
	GITBRANCH = ""
	// VERSION means release version
	VERSION = "0.0.0"
	// ROOTPATH is project root
	ROOTPATH = fmt.Sprintf("%s/src/github.com/alokic/%s", os.Getenv("GOPATH"), "spanner")
)

var (
	// rootCmd represents the base command when called without any subcommands
	rootCmd = &cobra.Command{
		Use:   "spanner",
		Short: "Spanner is proxy to all environments in Qubole.",
		Long: `Spanner routes request to a specifc environment based on user's credentials
		and environment selected. For example:
	
	Every request should have environment as paramenter to route.`,
	}
)

func buildVars() string {
	return fmt.Sprintf("git-commit, %s, git-branch, %s, version, %s", GITCOMMIT, GITBRANCH, VERSION)
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.spanner.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".spanner" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".spanner")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
