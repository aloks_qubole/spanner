// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"
	"time"

	"github.com/alokic/spanner/internal/filters"
	"github.com/alokic/spanner/internal/filters/post"
	"github.com/alokic/spanner/internal/filters/pre"

	"github.com/alokic/gopkg/sql"

	"github.com/alokic/gopkg/auth"
	cfg "github.com/alokic/gopkg/config"
	"github.com/alokic/gopkg/httputils"
	"github.com/alokic/spanner/cmd/service"
	"github.com/alokic/spanner/pkg/oauth2"
	"github.com/alokic/spanner/pkg/user"
	"github.com/bugsnag/bugsnag-go"
	"github.com/go-kit/kit/log"
	"github.com/gomodule/redigo/redis"
	"github.com/heptio/workgroup"
	newrelic "github.com/newrelic/go-agent"
	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	ioauth2 "golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// Svc struct
type Svc struct {
	Config        *Config
	Logger        log.Logger
	ServerName    string
	StatsDClient  *service.StatsD
	Redis         *redis.Pool
	MySQL         *sql.DB
	Newrelic      newrelic.Application
	Router        httputils.Router
	Proxy         *service.Proxy
	BugsnagConfig bugsnag.Configuration
	Oauth2        oauth2.Service
	User          user.Service
	UserJWT       *auth.JWT
	ServiceJWT    *auth.JWT
	Filters       *filters.Filters
}

// Config struct
type Config struct {
	Env                       string        `json:"env" usage:"environment of service" required:"true" `
	DBURL                     string        `json:"db_url" usage:"mysql database url" required:"false" print:"false"`
	UserJWTKey                string        `json:"user_jwt_key" usage:"jwt key to encode outh2 state and user jwttoken" required:"true" print:"false"`
	ServiceJWTKey             string        `json:"service_jwt_key" usage:"jwt key to encode service jwttoken" required:"true" print:"false"`
	RedisURL                  string        `json:"redis_url" usage:"redis url" required:"true" print:"false"`
	NewrelicKey               string        `json:"newrelic_key" usage:"newrelic license key" required:"true" print:"false"`
	BugsnagKey                string        `json:"bugsnag_key" usage:"bugsnag key" required:"true" print:"false"`
	RouterName                string        `json:"router_name" usage:"router name" required:"true" print:"false"`
	GoogleClientID            string        `json:"google_client_id" usage:"google client id for oauth2" required:"true" print:"false"`
	GoogleClientSecret        string        `json:"google_client_secret" usage:"google client secret for oauth2" required:"true" print:"false"`
	GoogleOauthCallbackURL    string        `json:"google_oauth_callback_url" usage:"google callback url for oauth2" required:"true"`
	Upstreams                 []interface{} `json:"upstreams" usage:"upstreams"`
	RoutingRules              []interface{} `json:"routing_rules" usage:"routing rules"`
	StatsDHost                string        `json:"statsd_host" usage:"statsd host url"`
	StatsDPort                int           `json:"statsd_port" usage:"statsd host port"`
	StatsDFlushInterval       int           `json:"statsd_flush_interval" usage:"statsd flush interval in seconds"`
	HTTPPort                  int           `json:"http_port" usage:"proxy server http port number"`
	HTTPSPort                 int           `json:"https_port" usage:"proxy server https port number"`
	DBMaxOpenConns            int           `json:"db_open_conns" usage:"max open connections in database"`
	DBMaxIdleConns            int           `json:"max_db_idle_conns" usage:"max open connections in database"`
	DBConnectRetryCount       int           `json:"db_connect_retry_count" usage:"number of times to retry to connect to database"`
	ServerDrainTime           int           `json:"server_drain_time" usage:"number of seconds needed for server to shutdown"`
	Oauth2TokenVerifyInterval int           `json:"oauth2_token_verify_interval" usage:"number of seconds after which token will be verified from provider(google) to see if its revoked or refreshed."`
	Debug                     bool          `json:"debug" usage:"enable pprof to debug"`
}

var (
	defaultConfig = &Config{
		RedisURL:                  "redis://localhost:6379",
		DBMaxOpenConns:            10,
		DBMaxIdleConns:            2,
		HTTPPort:                  4000,
		HTTPSPort:                 4001,
		DBConnectRetryCount:       5,
		ServerDrainTime:           5,
		StatsDHost:                "127.0.0.1",
		StatsDPort:                8125,
		StatsDFlushInterval:       10,
		RouterName:                "httprouter",
		GoogleOauthCallbackURL:    "http://localhost:4000/google_oauth_callback",
		Oauth2TokenVerifyInterval: 60,
	}
	svc = &Svc{
		Config:     defaultConfig,
		ServerName: "spanner",
		Router:     httputils.CreateRouter(defaultConfig.RouterName),
	}
)

var (
	svcConfigErr error
	svcCfg       *cfg.Config
	serviceCmd   = &cobra.Command{
		Use:   "service",
		Short: "Launh spanner webservice",
		Long: `Launches spanner webservice. For example:
	
	For help try:
	spanner service --help`,
		Run: func(cmd *cobra.Command, args []string) {
			err := svc.init()
			if err != nil {
				svc.Logger.Log("error", err, "exiting", "...")
				os.Exit(1)
			}

			svcCfg.Print(svc.Logger)

			err = svc.run()
			if err != nil {
				svc.Logger.Log("error", err, "exiting", "...")
				os.Exit(1)
			}
		},

		PreRunE: func(cmd *cobra.Command, args []string) error {
			return svcConfigErr
		},
	}
)

func init() {
	rootCmd.AddCommand(serviceCmd)
	svcConfigErr = svc.initConfig(serviceCmd.Flags())
}

// svcInitConfig loads config.
func (s *Svc) initConfig(flags *pflag.FlagSet) error {
	svcCfg = cfg.New(svc.Config, svc.ServerName, flags, fmt.Sprintf("%s/config", ROOTPATH))
	return svcCfg.Load()
}

// init service.
func (s *Svc) init() error {
	var err error

	s.initLogger()
	s.initStatsD()
	s.initBugsnag()

	err = s.initNewrelic()
	if err != nil {
		return err
	}

	err = s.initRedis()
	if err != nil {
		return err
	}

	err = s.initMySQL()
	if err != nil {
		return err
	}

	s.initJWT()
	s.initUserService()
	s.initOauth2Service()

	s.initFilters()
	return s.initProxy()
}

// run service
func (s *Svc) run() error {
	var g workgroup.Group

	err := service.HTTPServer(&g, &service.HTTPServerInput{
		Port: s.Config.HTTPPort, Router: s.Router, Logger: s.Logger,
		Exception: s.BugsnagConfig, APM: s.Newrelic,
		NotFoundHandler: s.Proxy, OauthProvider: s.Oauth2,
	})

	if err != nil {
		return err
	}

	s.Logger.Log(buildVars())
	s.Logger.Log("workgroup", "shutdown", "status", g.Run())
	return nil
}

func (s *Svc) initLogger() {
	s.Logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	s.Logger = log.With(s.Logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller, "app", s.ServerName)
}

func (s *Svc) initOauth2Service() {
	s.Oauth2 = oauth2.NewService(&oauth2.Input{
		UserInfoURL: "https://openidconnect.googleapis.com/v1/userinfo",
		Provider:    "google",
		Config: &ioauth2.Config{
			RedirectURL:  s.Config.GoogleOauthCallbackURL,
			ClientID:     s.Config.GoogleClientID,
			ClientSecret: s.Config.GoogleClientSecret,
			Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email", "https://www.googleapis.com/auth/userinfo.profile"},
			Endpoint:     google.Endpoint,
		},
		TokenVerifyInterval: time.Duration(s.Config.Oauth2TokenVerifyInterval) * time.Second,
		JWT:                 s.UserJWT,
		Oauth2Repo:          oauth2.NewMysql(s.MySQL),
		UserRepo:            user.NewMysql(s.MySQL),
	})
}

func (s *Svc) initUserService() {
	s.User = user.NewService(user.NewMysql(s.MySQL), s.UserJWT)
}

func (s *Svc) initJWT() {
	s.UserJWT = auth.NewJWT(s.Config.UserJWTKey)
	s.ServiceJWT = auth.NewJWT(s.Config.ServiceJWTKey)
}

func (s *Svc) initMySQL() error {
	db, err := service.NewMySQL(&service.MySQLInput{
		ConnectRetryCount: s.Config.DBConnectRetryCount,
		MaxIdleConns:      s.Config.DBMaxIdleConns,
		MaxOpenConns:      s.Config.DBMaxOpenConns,
		URL:               s.Config.DBURL,
		Logger:            s.Logger,
	})

	s.MySQL = db
	return err
}

func (s *Svc) initRedis() error {
	r, err := service.NewRedis(&service.RedisInput{
		URL:    s.Config.RedisURL,
		Logger: s.Logger,
	})
	s.Redis = r
	return err
}

func (s *Svc) initProxy() error {
	p, err := service.NewProxy(&service.ProxyInput{
		RoutingRules: s.Config.RoutingRules, Upstreams: s.Config.Upstreams, Filters: s.Filters,
	})
	s.Proxy = p
	return err
}

func (s *Svc) initNewrelic() error {
	nr, err := service.NewNewrelic(&service.NewrelicInput{
		Server: s.ServerName, Env: s.Config.Env, License: s.Config.NewrelicKey,
	})
	s.Newrelic = nr
	return err
}

func (s *Svc) initBugsnag() {
	s.BugsnagConfig = service.NewBugsnag(&service.BugsnagInput{
		Pkg: fmt.Sprintf("%s/cmd/service", ROOTPATH), Env: s.Config.Env, Key: s.Config.BugsnagKey, Logger: s.Logger,
	})
}

func (s *Svc) initStatsD() {
	s.StatsDClient = service.NewStatsD(&service.StatsDInput{
		Prefix: fmt.Sprintf("spanner-%s", s.Config.Env), FlushInterval: s.Config.StatsDFlushInterval,
		Host: s.Config.StatsDHost, Port: s.Config.StatsDPort, Logger: s.Logger,
	})
}

func (s *Svc) initFilters() {
	f := filters.New()
	f.SetPre("validate_token", pre.ValidateToken(s.Oauth2, s.ServiceJWT))
	f.SetPost("rewrite_body_nop", post.RewriteBodyNop)
	f.SetPost("redirect_localhost_to_http", post.RedirectLocalhostToHTTP)
	s.Filters = f
}
