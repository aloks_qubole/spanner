// Copyright © 2019 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"os"

	"github.com/alokic/gopkg/sql"
	"github.com/alokic/spanner/cmd/service"
	"github.com/alokic/spanner/pkg/mysql"
	"github.com/go-kit/kit/log"
	"github.com/spf13/cobra"
)

// Seed struct
type Seed struct {
	MySQL  *sql.DB
	Logger log.Logger
	DBURL  string
}

var (
	seed = &Seed{}

	// seedCmd represents the seed command
	seedCmd = &cobra.Command{
		Use:   "seed",
		Short: "Seed a mysql database.",
		Long: `This command seeds a mysql database. 
				For example:

			cli seed --db_url <mysql_db_url>`,
		Run: func(cmd *cobra.Command, args []string) {
			seed.init()

			mysql.Clear(seed.MySQL, seed.Logger)
			mysql.Seed(seed.MySQL, fmt.Sprintf("%s/internal/mysql/seed", ROOTPATH), seed.Logger)
			fmt.Println("seed success")
		},
	}
)

func (s *Seed) init() error {
	var err error

	s.initLogger()

	s.MySQL, err = service.NewMySQL(&service.MySQLInput{
		ConnectRetryCount: 5,
		MaxIdleConns:      5,
		MaxOpenConns:      5,
		URL:               s.DBURL,
		Logger:            s.Logger,
	})

	return err
}

func (s *Seed) initLogger() {
	s.Logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	s.Logger = log.With(s.Logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller, "app", "seed")
}

func init() {
	rootCmd.AddCommand(seedCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// seedCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// seedCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	seedCmd.Flags().StringVar(&seed.DBURL, "db_url", "", "mysql database url (required)")
	seedCmd.MarkFlagRequired("db_url")
}
