package user

import (
	"context"
	"fmt"
	"time"

	"github.com/alokic/gopkg/sql"
	"github.com/alokic/gopkg/timeutils"
	"github.com/alokic/spanner/pkg/mysql"
	"github.com/pkg/errors"
)

var (
	// MysqlGetQuery is get query template to get an user.
	MysqlGetQuery = "SELECT * FROM users WHERE id = ?"
	// MysqlListQuery is list query template for listing users.
	MysqlListQuery = "SELECT * FROM users WHERE id IN (?)"
	// MysqlGetByEmailQuery is get by email query template to get an user.
	MysqlGetByEmailQuery = "SELECT * FROM users WHERE email = ?"
	// MysqlUpdateQuery is update query template to update an user.
	MysqlUpdateQuery = fmt.Sprintf("UPDATE users SET %s WHERE %s", "%s", "%s")
)

// Mysql is postgres repo for users.
type Mysql struct {
	db        mysql.DB
	fieldInfo *sql.FieldInfo
}

type mysqlAttrib struct {
	ID        uint64    `db:"id"`
	Name      string    `db:"name"  valid:"required"`
	Email     string    `db:"email" valid:"required;email"`
	State     string    `db:"state" valid:"required"`
	CreatedAt time.Time `db:"created_at"`
	UpdatedAt time.Time `db:"updated_at"`
}

// NewMysql is is user constructor.
func NewMysql(d mysql.DB) *Mysql {
	return &Mysql{db: d, fieldInfo: sql.GenFieldInfo(mysql.DriverName, mysqlAttrib{})}
}

// Create a user in db.
func (repo *Mysql) Create(ctx context.Context, usr *User) (*User, error) {
	t := toMysqlAttrib(usr)

	// t.CreatedAt = time.Now().UTC()
	// t.UpdatedAt = t.CreatedAt

	err := repo.validate(t)
	if err != nil {
		return nil, errors.Wrap(err, "user:mysql:create:validate")
	}

	res, err := mysql.BatchInsert(repo.db, "users", []interface{}{t}, repo.fieldInfo)
	if err != nil {
		return nil, errors.Wrap(err, "user:mysql:create:batch_insert")
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, errors.Wrap(err, "user:mysql:create:last_insert_id")
	}

	t.ID = uint64(id)
	return t.toUser(), nil
}

// Get an user.
func (repo *Mysql) Get(ctx context.Context, id uint64) (*User, error) {
	var t []mysqlAttrib

	if err := repo.db.Select(&t, MysqlGetQuery, id); err != nil {
		return nil, errors.Wrap(err, "user:mysql:get:select")
	}

	if len(t) == 0 {
		return nil, nil
	}
	return t[0].toUser(), nil
}

// GetByEmail gets an user by email.
func (repo *Mysql) GetByEmail(ctx context.Context, email string) (*User, error) {
	var t []mysqlAttrib

	if err := repo.db.Select(&t, MysqlGetByEmailQuery, email); err != nil {
		return nil, errors.Wrap(err, "user:mysql:get_by_email:select")
	}

	if len(t) == 0 {
		return nil, nil
	}
	return t[0].toUser(), nil
}

// List users.
func (repo *Mysql) List(ctx context.Context, ids []uint64) ([]*User, error) {
	if len(ids) == 0 {
		return nil, nil
	}

	query, args, err := sql.In(MysqlListQuery, ids)
	if err != nil {
		return nil, errors.Wrap(err, "user:mysql:list:in")
	}

	var ts []*mysqlAttrib
	if err := repo.db.Select(&ts, query, args...); err != nil {
		return nil, errors.Wrap(err, "user:mysql:list:select")
	}

	var e []*User
	for _, v := range ts {
		e = append(e, v.toUser())
	}

	return e, nil
}

// Update an user.
func (repo *Mysql) Update(ctx context.Context, usr *User) error {
	mysqlAttrib := toMysqlAttrib(usr)

	if err := repo.update(ctx, mysqlAttrib); err != nil {
		return errors.Wrap(err, "user:mysql:update")
	}

	return nil
}

//
func (repo *Mysql) update(ctx context.Context, p *mysqlAttrib, args ...string) error {
	var condition string
	{
		if len(args) == 0 {
			condition = fmt.Sprintf("id = %d", p.ID)
		} else {
			condition = args[0]
		}
	}

	var setStatement string
	{
		s, err := sql.PartialUpdateStmt(p, "users", condition, repo.fieldInfo)
		if err != nil {
			return err
		}
		setStatement = s
	}

	_, err := repo.db.Exec(setStatement)
	return err
}

func (t *mysqlAttrib) toUser() *User {
	e := &User{}
	if t == nil {
		return e
	}

	e.Id = t.ID
	e.Name = t.Name
	e.Email = t.Email
	e.State = t.State
	e.CreatedAt, _ = timeutils.ToProto(t.CreatedAt)
	e.UpdatedAt, _ = timeutils.ToProto(t.UpdatedAt)

	return e
}

// toMysqlAttrib entity gateway
func toMysqlAttrib(t *User) *mysqlAttrib {
	e := &mysqlAttrib{}
	if t == nil {
		return e
	}

	e.ID = t.Id
	e.Name = t.Name
	e.Email = t.Email
	e.State = t.State

	return e
}

func (repo *Mysql) validate(p *mysqlAttrib, validateOnlyPresent ...bool) error {
	_, err := repo.fieldInfo.Validate(p, validateOnlyPresent...)

	return err
}
