package user

import (
	"context"

	"github.com/pkg/errors"
)

var (
	// MaskedPassword is template for masked password.
	MaskedPassword = "MASKED-PASSWORD"
	// MsgBlocked means user is blocked by administrator.
	MsgBlocked = "Your account is blocked. Please contact administartor."
	// MsgSignedOut means user is signed out and needs to sign in to continue.
	MsgSignedOut = "You are signed out. Please sign in."

	// ErrUserBlocked means user is blocked.
	ErrUserBlocked = errors.New("user is blocked")
	// ErrNoSelfActivate means user is trying to activate themself.
	ErrNoSelfActivate = errors.New("user cannot activate themself")
	// ErrNoSelfBlock means user is trying to block themself.
	ErrNoSelfBlock = errors.New("user cannot block themself")
)

// JWTer is interface for a JWT tokenization
type JWTer interface {
	Generate(map[string]interface{}) (string, error)
}

type service struct {
	db  Repository
	jwt JWTer
}

// NewService is constructor for user service.
func NewService(db Repository, jwt JWTer) Service {
	return &service{db: db, jwt: jwt}
}

// List usecase for service.
func (s *service) List(ctx context.Context, in *ListRequest) (*ListResponse, error) {
	us, err := s.db.List(ctx, in.Ids)
	if err != nil {
		return nil, errors.Wrap(err, "user.service.list")
	}

	return &ListResponse{Users: us}, nil
}

// Get usecase for service.
func (s *service) Get(ctx context.Context, in *GetRequest) (*GetResponse, error) {
	u, err := s.db.Get(ctx, in.Id)
	if err != nil {
		return nil, err
	}

	return &GetResponse{User: u}, nil
}

// Block usecase for service.
func (s *service) Block(ctx context.Context, in *BlockRequest) (*BlockResponse, error) {
	if in.BlockId == in.Id {
		return nil, ErrNoSelfBlock
	}

	if err := s.db.Update(ctx, &User{Id: in.BlockId, State: StateBlocked}); err != nil {
		return nil, err
	}

	return &BlockResponse{Result: true}, nil
}

// Activate usecase for service.
func (s *service) Activate(ctx context.Context, in *ActivateRequest) (*ActivateResponse, error) {
	if in.ActivateId == in.Id {
		return nil, ErrNoSelfActivate
	}
	if err := s.db.Update(ctx, &User{Id: in.ActivateId, State: StateActive}); err != nil {
		return nil, err
	}

	return &ActivateResponse{Result: true}, nil
}
