package user

import (
	"context"
	"regexp"

	"github.com/pkg/errors"
)

var (
	// ErrInvalidEmail means email is in invalid format(check regex) or blank.
	ErrInvalidEmail = errors.New("invalid email")
	// ErrNameBlank means name is blank.
	ErrNameBlank = errors.New("name is blank")
)

var (
	// StateActive means user is active in system.
	StateActive = "active"
	// StateBlocked means user is blocked.
	StateBlocked = "blocked"
)

var (
	emailRegex = "^[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[a-zA-Z0-9](?:[\\w-]*[\\w])?$"
)

// Users is list of domain objects.
type Users []*User

// Repository is interface for a user store
type Repository interface {
	Get(context.Context, uint64) (*User, error)
	GetByEmail(context.Context, string) (*User, error)
	List(context.Context, []uint64) ([]*User, error)
	Update(context.Context, *User) error
}

// Service provides interface for user service.
type Service ServiceServer

// New is factory for user.
func New(name, email string) (*User, error) {
	if err := valid("email", email); err != nil {
		return nil, err
	}

	if err := valid("name", name); err != nil {
		return nil, err
	}

	return &User{
		Name: name, Email: email, State: StateActive,
	}, nil
}

func valid(key, val string) error {
	switch key {
	case "email":
		b, err := regexp.MatchString(emailRegex, val)
		if err != nil {
			return err
		}

		if !b {
			return ErrInvalidEmail
		}
	case "name":
		if val == "" {
			return ErrNameBlank
		}
	}
	return nil
}
