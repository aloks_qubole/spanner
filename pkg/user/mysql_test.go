package user_test

import (
	"context"
	"strings"
	"testing"

	"github.com/alokic/gopkg/sliceutils"
	"github.com/alokic/gopkg/typeutils"
	"github.com/alokic/spanner/pkg/mysql"
	"github.com/alokic/spanner/pkg/testhelper"
	"github.com/alokic/spanner/pkg/user"
	"github.com/pkg/errors"
)

type testUserConf struct {
	db *testhelper.Tx
}

type testComparator func(interface{}, interface{}) bool

func TestMysql_Create(t *testing.T) {
	conf, teardown := testSetup()
	defer teardown()

	type args struct {
		ctx context.Context
		usr *user.User
	}
	tests := []struct {
		name         string
		args         args
		want         *user.User
		wantErr      bool
		wantErrValue error
	}{
		{
			name:         "TestSuccess",
			args:         args{ctx: context.Background(), usr: testNewUser("test1", "a@b.c")},
			want:         testNewUser("test1", "a@b.c"),
			wantErr:      false,
			wantErrValue: nil,
		},
		{
			name:         "TestDuplicateEmail",
			args:         args{ctx: context.Background(), usr: testNewUser("test1", "duplicate@b.c")},
			want:         nil,
			wantErr:      true,
			wantErrValue: errors.New("user:mysql:create:batch_insert"),
		},
		{
			name:         "TestInvalidEmail",
			args:         args{ctx: context.Background(), usr: testNewUser("test1", "invalid")},
			want:         nil,
			wantErr:      true,
			wantErrValue: errors.New("user:mysql:create:validate"),
		},
		{
			name:         "TestEmptyName",
			args:         args{ctx: context.Background(), usr: testNewUser("", "a@b.c")},
			want:         nil,
			wantErr:      true,
			wantErrValue: errors.New("user:mysql:create:validate"),
		},
	}

	testCreateUser(conf.db, testNewUser("test1", "duplicate@b.c"))

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := user.NewMysql(conf.db)

			got, err := repo.Create(tt.args.ctx, tt.args.usr)
			if (err != nil) != tt.wantErr {
				t.Errorf("Mysql.Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if err != nil && !testCompare(err, tt.wantErrValue, func(e1, e2 interface{}) bool {
				return strings.Contains(e1.(error).Error(), e2.(error).Error())
			}) {
				t.Errorf("Mysql.Create() errorValue = %v, wantErrValue %v", err, tt.wantErrValue)
				return
			}

			if !testCompare(got, tt.want, func(e1, e2 interface{}) bool {
				u1, _ := e1.(*user.User)
				u2, _ := e2.(*user.User)
				return u1.Email == u2.Email && u1.Name == u2.Name && u1.State == u2.State
			}) {
				t.Errorf("Mysql.Create() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMysql_Get(t *testing.T) {
	conf, teardown := testSetup()
	defer teardown()

	u1, _ := testCreateUser(conf.db, testNewUser("test1", "a@b.c", 100))

	type args struct {
		ctx context.Context
		id  uint64
	}
	tests := []struct {
		name         string
		args         args
		want         *user.User
		wantErr      bool
		wantErrValue error
	}{
		{
			name:         "TestSuccess",
			args:         args{ctx: context.Background(), id: u1.Id},
			want:         u1,
			wantErr:      false,
			wantErrValue: nil,
		},
		{
			name:         "TestNonExistent",
			args:         args{ctx: context.Background(), id: 1000},
			want:         nil,
			wantErr:      false,
			wantErrValue: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := user.NewMysql(conf.db)

			got, err := repo.Get(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Mysql.Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if err != nil && !testCompare(err, tt.wantErrValue, func(e1, e2 interface{}) bool {
				return strings.Contains(e1.(error).Error(), e2.(error).Error())
			}) {
				t.Errorf("Mysql.Get() errorValue = %v, wantErrValue %v", err, tt.wantErrValue)
				return
			}

			if !testCompare(got, tt.want, testCompareUser) {
				t.Errorf("Mysql.Get() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMysql_GetByEmail(t *testing.T) {
	conf, teardown := testSetup()
	defer teardown()

	u1, _ := testCreateUser(conf.db, testNewUser("test1", "a@b.c", 100))

	type args struct {
		ctx   context.Context
		email string
	}
	tests := []struct {
		name         string
		args         args
		want         *user.User
		wantErr      bool
		wantErrValue error
	}{
		{
			name:         "TestSuccess",
			args:         args{ctx: context.Background(), email: u1.Email},
			want:         u1,
			wantErr:      false,
			wantErrValue: nil,
		},
		{
			name:         "TestNonExistent",
			args:         args{ctx: context.Background(), email: "notexists@b.c"},
			want:         nil,
			wantErr:      false,
			wantErrValue: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := user.NewMysql(conf.db)

			got, err := repo.GetByEmail(tt.args.ctx, tt.args.email)
			if (err != nil) != tt.wantErr {
				t.Errorf("Mysql.GetByEmail() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if err != nil && !testCompare(err, tt.wantErrValue, func(e1, e2 interface{}) bool {
				return strings.Contains(e1.(error).Error(), e2.(error).Error())
			}) {
				t.Errorf("Mysql.GetByEmail() errorValue = %v, wantErrValue %v", err, tt.wantErrValue)
				return
			}

			if !testCompare(got, tt.want, testCompareUser) {
				t.Errorf("Mysql.GetByEmail() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMysql_List(t *testing.T) {
	conf, teardown := testSetup()
	defer teardown()

	u1, _ := testCreateUser(conf.db, testNewUser("test1", "a@b.c", 100))
	u2, _ := testCreateUser(conf.db, testNewUser("test2", "b@b.c", 200))

	type args struct {
		ctx context.Context
		ids []uint64
	}
	tests := []struct {
		name         string
		args         args
		want         []*user.User
		wantErr      bool
		wantErrValue error
	}{
		{
			name:         "TestSuccess",
			args:         args{ctx: context.Background(), ids: []uint64{u1.Id, u2.Id}},
			want:         []*user.User{u2, u1},
			wantErr:      false,
			wantErrValue: nil,
		},
		{
			name:         "TestEmptyList",
			args:         args{ctx: context.Background(), ids: nil},
			want:         nil,
			wantErr:      false,
			wantErrValue: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := user.NewMysql(conf.db)

			got, err := repo.List(tt.args.ctx, tt.args.ids)
			if (err != nil) != tt.wantErr {
				t.Errorf("Mysql.List() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if err != nil && !testCompare(err, tt.wantErrValue, func(e1, e2 interface{}) bool {
				return strings.Contains(e1.(error).Error(), e2.(error).Error())
			}) {
				t.Errorf("Mysql.List() errorValue = %v, wantErrValue %v", err, tt.wantErrValue)
				return
			}

			fn := func(e1, e2 interface{}) bool {
				intersection := sliceutils.Intersect(e1, e2, testCompareUser).([]interface{})
				return len(e1.([]*user.User)) == len(e1.([]*user.User)) && len(intersection) == len(e1.([]*user.User))
			}

			if !testCompare(got, tt.want, fn) {
				t.Errorf("Mysql.List() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMysql_Update(t *testing.T) {
	conf, teardown := testSetup()
	defer teardown()

	usr, _ := testCreateUser(conf.db, testNewUser("test1", "a@b.c", 100))

	type args struct {
		ctx context.Context
		usr *user.User
	}
	tests := []struct {
		name    string
		args    args
		want    error
		wantErr bool
		oldUser *user.User
	}{
		{
			name: "TestSuccess",
			args: args{ctx: context.Background(), usr: func() *user.User {
				return &user.User{Id: usr.Id, Name: "test2", Email: usr.Email}
			}()},
			want:    nil,
			wantErr: false,
			oldUser: usr,
		},
		{
			name:    "TestNoExistentUser",
			args:    args{ctx: context.Background(), usr: testNewUser("test1", "notexist@b.c")},
			want:    nil,
			wantErr: false,
			oldUser: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := user.NewMysql(conf.db)

			err := repo.Update(tt.args.ctx, tt.args.usr)
			if (err != nil) != tt.wantErr {
				t.Errorf("Mysql.Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if err != nil && !testCompare(err, tt.want, func(e1, e2 interface{}) bool {
				return strings.Contains(e1.(error).Error(), e2.(error).Error())
			}) {
				t.Errorf("Mysql.Update().CompareError errorValue = %v, wantErr %v", err, tt.wantErr)
				return
			}

			gotUser, err := repo.GetByEmail(tt.args.ctx, tt.args.usr.Email)
			if err != nil {
				t.Errorf("Mysql.Update().GetByEmail err = %v, gotUser = %v, oldUser %v", err, gotUser, tt.args.usr)
				return
			}

			if !testCompare(gotUser, tt.oldUser, func(e1, e2 interface{}) bool {
				u1, _ := e1.(*user.User)
				u2, _ := e2.(*user.User)
				return u1.Email == u2.Email && u1.Name != u2.Name && u1.Id == u2.Id && (u1.UpdatedAt.Seconds > u2.UpdatedAt.Seconds)
			}) {
				t.Errorf("Mysql.Update().CompareError gotUser = %v, oldUser %v", gotUser, tt.oldUser)
				return
			}
		})
	}
}

func testCompare(e1, e2 interface{}, fn testComparator) bool {
	if typeutils.Blank(e1) && typeutils.Blank(e2) {
		return true
	}

	if typeutils.Blank(e1) || typeutils.Blank(e2) {
		return false
	}

	return fn(e1, e2)
}

func testCompareUser(e1, e2 interface{}) bool {
	u1, _ := e1.(*user.User)
	u2, _ := e2.(*user.User)
	return u1.Id == u2.Id && u1.Email == u2.Email && u1.Name == u2.Name && u1.State == u2.State
}

func testNewUser(name, email string, id ...uint64) *user.User {
	p, err := user.New(name, email)
	if err != nil {
		return nil
	}
	if len(id) > 0 {
		p.Id = id[0]
	}
	return p
}

func testCreateUser(db mysql.DB, p *user.User) (*user.User, error) {
	repo := user.NewMysql(db)
	return repo.Create(context.Background(), p)
}

func testSetup() (*testUserConf, func()) {
	db := testhelper.GetMysql()
	db.SeedDB()
	return &testUserConf{db: db}, func() {
		db.CleanDB()
	}
}
