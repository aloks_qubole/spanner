package testhelper

import (
	"fmt"
	"os"
	"time"

	aredis "github.com/alokic/gopkg/redis"
	sql "github.com/alokic/gopkg/sql"
	"github.com/alokic/gopkg/stringutils"
	"github.com/alokic/spanner/pkg/mysql"
	"github.com/go-kit/kit/log"
	"github.com/gomodule/redigo/redis"
	"github.com/jmoiron/sqlx/reflectx"
)

// SvcConfig defines config need for testing.
type SvcConfig struct {
	DBTestURL           string `json:"db_test_url" usage:"mysql database url" required:"true"`
	JWTKey              string `json:"jwt_key" usage:"jwt key for authentication" required:"true"`
	RedisTestURL        string `json:"redis_url" usage:"redis url" required:"true"`
	HTTPPort            int    `json:"http_port" usage:"http port number"`
	HTTPSPort           int    `json:"https_port" usage:"https port number"`
	DBMaxOpenConns      int    `json:"db_open_conns" usage:"max open connections in database"`
	DBMaxIdleConns      int    `json:"max_db_idle_conns" usage:"max open connections in database"`
	DBConnectRetryCount int    `json:"db_connect_retry_count" usage:"number of times to retry to connect to database"`
	RouterName          string `json:"router_name" usage:"name of the router. 'gorilla' router supported as of now."`
}

var (
	// cfg defaults
	cfg = &SvcConfig{
		DBTestURL:           os.Getenv("SPANNER_DB_TEST_URL"),
		JWTKey:              os.Getenv("SPANNER_JWT_KEY"),
		RedisTestURL:        os.Getenv("SPANNER_REDIS_TEST_URL"),
		DBMaxOpenConns:      10,
		DBMaxIdleConns:      2,
		HTTPPort:            4000,
		HTTPSPort:           4001,
		DBConnectRetryCount: 5,
		RouterName:          "httprouter",
	}
	serverName  = "spanner"
	projectRoot = fmt.Sprintf("%s/src/github.com/alokic/%s", os.Getenv("GOPATH"), serverName)
	logger      log.Logger
	mysqlDB     *DB
	cfgInit     bool
)

// GetConfig returns test configs.
func GetConfig() *SvcConfig {
	return cfg
}

// nilLogger does not print.
type nilLogger struct{}

// Log interface implemnetation.
func (l *nilLogger) Log(args ...interface{}) error {
	return nil
}

// NilLogger does not print.
func NilLogger() log.Logger {
	return &nilLogger{}
}

// GetLogger returns  a logger to be used.
func GetLogger() log.Logger {
	if logger == nil {
		initLogger()
	}
	return logger
}

// GetMysql returns a mysql db to be used.
func GetMysql() *Tx {
	if mysqlDB == nil {
		mysqlDB = initMysql(cfg.DBTestURL)
	}

	tx := mysqlDB.Begin()
	tx.Mapper = reflectx.NewMapperFunc("db", func(s string) string {
		return stringutils.ToLowerSnakeCase(s)
	})

	return &Tx{Tx: tx}
}

// GetRedis returns a redis db to be used
func GetRedis() *Rs {
	r := initRedis(cfg.RedisTestURL)
	return &Rs{Pool: r}
}

// DB struct.
type DB struct {
	*sql.DB
}

// Tx struct.
type Tx struct {
	*sql.Tx
}

// Rs struct for redis.
type Rs struct {
	Conn redis.Conn
	*redis.Pool
}

//CleanRedis will clean existing keys in redis.
func (r *Rs) CleanRedis() {
	conn := r.Conn
	defer conn.Close()
	_, err := conn.Do("FLUSHDB")

	if err != nil {
		logger.Log("redis", "flush", "error", err, "exiting", "...")
	}
}

//NewDB won't attempt connection to DB unless needed. Callers can call Ping() to test connection.
func NewDB(driver, url string) (*DB, error) {
	db, err := sql.NewDB(driver, url)
	if err != nil {
		return nil, err
	}

	return &DB{DB: db}, nil
}

// SeedDB database in transaction.
func (t *Tx) SeedDB() {
	mysql.Seed(t, fmt.Sprintf("%s/pkg/mysql/seed", projectRoot), NilLogger())
}

// CleanDB database in transaction.
func (t *Tx) CleanDB() {
	t.Rollback()
}

// CleanDB database.
func CleanDB() {
	mysql.Clear(mysqlDB, NilLogger())
}

// SeedDB database.
func SeedDB() {
	mysql.Seed(mysqlDB, fmt.Sprintf("%s/pkg/mysql/seed", projectRoot), NilLogger())
}

// initLogger inititialize logger
func initLogger() {
	logger = log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)
}

// initMysql connects to test database
func initMysql(dburl ...string) *DB {
	var err error
	if len(dburl) > 0 {
		cfg.DBTestURL = dburl[0]
	}

	if cfg.DBTestURL == "" {
		logger.Log("test", "database", "not", "found")
		os.Exit(1)
	}

	for count := 0; count < cfg.DBConnectRetryCount; count++ {
		d, err := NewDB("mysql", cfg.DBTestURL)

		if err == nil {
			err = d.DB.Ping()
		}
		if err != nil {
			logger.Log("mysql", "connect", "error", err, "retry", count < cfg.DBConnectRetryCount)
			time.Sleep(2 * time.Second)
			continue
		}
		// this converts all struct  name to snakecase name to map to mysql db column
		d.DB.MapperFunc(func(s string) string {
			return stringutils.ToLowerSnakeCase(s)
		})

		d.DB.SetMaxIdleConns(5)
		d.DB.SetMaxOpenConns(30)
		return d
	}
	logger.Log("mysql", "connect", "error", err, "exiting", "...")
	os.Exit(1)
	return nil
}

func initRedis(redisURL string) *redis.Pool {
	return aredis.NewRedisPool(redisURL)
}
