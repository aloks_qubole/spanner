package mysql

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"

	dsql "database/sql"

	"github.com/alokic/gopkg/sql"
	"github.com/pkg/errors"
)

var (
	// DriverName is type of sql driver used to access db.
	DriverName = "mysql"
)

// Logger is interface for logger in db.
type Logger interface {
	Log(...interface{}) error
}

// DB is sql.DB interface.
type DB interface {
	Exec(string, ...interface{}) (dsql.Result, error)
	Select(dest interface{}, query string, args ...interface{}) error
	Transaction(func(*sql.Tx) (interface{}, error)) error
}

// BatchInsert inserts in batch.
func BatchInsert(db DB, tableName string, records []interface{}, fi *sql.FieldInfo) (dsql.Result, error) {
	stmt, params := sql.BatchInsertStatement(tableName, records, fi)
	stmt = sql.Rebind(DriverName, stmt)
	return db.Exec(stmt, params...)
}

// BatchUpsert upserts in batch.
func BatchUpsert(db DB, tableName string, records []interface{}, fi *sql.FieldInfo) (dsql.Result, error) {
	stmt, params, err := sql.MysqlBatchUpsertStatement(tableName, records, fi)
	if err != nil {
		return nil, errors.Wrap(err, "mysql.batch_upsert")
	}
	stmt = sql.Rebind(DriverName, stmt)
	return db.Exec(stmt, params...)
}

// Clear db.
func Clear(d DB, logger Logger) {
	for _, v := range []string{"oauth2", "users"} {
		logger.Log("Clear", v, "error", clearTable(d, v))
	}
}

// Seed seeds user db.
func Seed(d DB, folder string, logger Logger) {
	for _, v := range []string{"users", "oauth2"} {
		logger.Log("Seed", v, "error", seed(d, v, fmt.Sprintf("%s/%s.json", folder, v)))
	}
}

func seed(d DB, name, file string) error {
	var ms []map[string]interface{}

	err := readSeedFile(file, &ms)
	if err != nil {
		return err
	}

	for _, m := range ms {
		cols, params := createQuery(m)
		_, err = d.Exec(fmt.Sprintf("INSERT into %s (%s) VALUES (%s)", name, cols, params))
		if err != nil {
			return err
		}
	}
	return nil
}

// clearTable removes data from table.
func clearTable(d DB, name string) error {
	_, err := d.Exec(fmt.Sprintf("DELETE from %s", name))
	return err
}

func createQuery(m map[string]interface{}) (string, string) {
	var cols []string
	var params []string

	for k, v := range m {
		cols = append(cols, k)
		params = append(params, fmt.Sprintf("'%v'", v))
	}
	return strings.Join(cols, ","), strings.Join(params, ",")
}

func readSeedFile(file string, v interface{}) error {
	b, err := ioutil.ReadFile(file)
	if err != nil {
		return err
	}
	return json.Unmarshal(b, v)
}
