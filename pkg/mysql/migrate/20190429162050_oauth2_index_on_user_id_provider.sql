-- +goose Up
-- +goose StatementBegin
CREATE UNIQUE INDEX idx_oauth2_user_id_provider ON oauth2(user_id, provider);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE oauth2 
  DROP FOREIGN KEY fk_oauth2_user_id_provider, 
  DROP INDEX idx_oauth2_user_id_provider,
  ADD CONSTRAINT fk_oauth2_user_id_provider_1  FOREIGN KEY (user_id) REFERENCES users (id);
-- +goose StatementEnd
