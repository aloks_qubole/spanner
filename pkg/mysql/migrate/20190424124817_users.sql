-- +goose Up
-- +goose StatementBegin
CREATE TABLE users (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  email VARCHAR(64) NOT NULL CHECK (email <> ''),
  name VARCHAR(64) NOT NULL CHECK (name <> ''),
  state VARCHAR(10) NOT NULL CHECK (state <> ''),
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);


-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE users;
-- +goose StatementEnd
