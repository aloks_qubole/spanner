-- +goose Up
-- +goose StatementBegin
CREATE TABLE oauth2 (
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  user_id BIGINT NOT NULL,
  provider VARCHAR(64) NOT NULL CHECK (provider <> ''),
  access_token VARCHAR(256) NOT NULL CHECK (access_token <> ''),
  refresh_token VARCHAR(256) NOT NULL CHECK (refresh_token <> ''),
  token_type VARCHAR(16) NOT NULL CHECK (token_type <> ''),
  state VARCHAR(16) NOT NULL CHECK (state <> ''),
  expiry TIMESTAMP NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  last_refreshed_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT fk_oauth2_user_id_provider  FOREIGN KEY (user_id) REFERENCES users (id)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE oauth2;
-- +goose StatementEnd
