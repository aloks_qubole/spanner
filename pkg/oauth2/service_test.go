package oauth2

import (
	"context"
	"reflect"
	"testing"

	"golang.org/x/oauth2"
)

func Test_service_SignUp(t *testing.T) {
	type fields struct {
		config      *oauth2.Config
		provider    string
		userInfoURL string
		jwt         JWTer
		oauth2Repo  Repository
		userRepo    UserRepository
	}
	type args struct {
		ctx context.Context
		in  *SignUpRequest
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *SignUpResponse
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service{
				config:      tt.fields.config,
				provider:    tt.fields.provider,
				userInfoURL: tt.fields.userInfoURL,
				jwt:         tt.fields.jwt,
				oauth2Repo:  tt.fields.oauth2Repo,
				userRepo:    tt.fields.userRepo,
			}
			got, err := s.SignUp(tt.args.ctx, tt.args.in)
			if (err != nil) != tt.wantErr {
				t.Errorf("service.SignUp() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("service.SignUp() = %v, want %v", got, tt.want)
			}
		})
	}
}
