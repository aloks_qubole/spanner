package oauth2

import (
	"context"
	"fmt"
	"time"

	"github.com/alokic/gopkg/sql"
	"github.com/alokic/gopkg/timeutils"
	"github.com/alokic/spanner/pkg/mysql"
	"github.com/pkg/errors"
)

var (
	// MysqlGetQuery is get query template to get an oauth2.
	MysqlGetQuery = "SELECT * FROM oauth2 WHERE id = ?"
	// MysqlUpdateQuery is update query template to update an oauth2.
	MysqlUpdateQuery = fmt.Sprintf("UPDATE oauth2 SET %s WHERE %s", "%s", "%s")
)

// Mysql is postgres repo for oauth2.
type Mysql struct {
	db        mysql.DB
	fieldInfo *sql.FieldInfo
}

type mysqlAttrib struct {
	ID              uint64    `db:"id"`
	UserID          uint64    `db:"user_id"`
	Provider        string    `db:"provider"`
	AccessToken     string    `db:"access_token"`
	RefreshToken    string    `db:"refresh_token"`
	TokenType       string    `db:"token_type"`
	State           string    `db:"state"`
	Expiry          time.Time `db:"expiry"`
	CreatedAt       time.Time `db:"created_at"`
	UpdatedAt       time.Time `db:"updated_at"`
	LastRefreshedAt time.Time `db:"last_refreshed_at"`
}

// NewMysql is is oauth2 constructor.
func NewMysql(d mysql.DB) *Mysql {
	return &Mysql{db: d, fieldInfo: sql.GenFieldInfo(mysql.DriverName, mysqlAttrib{})}
}

// Upsert a oauth2 in db.
func (repo *Mysql) Upsert(ctx context.Context, oa2 *Oauth2) (*Oauth2, error) {
	t := toMysqlAttrib(oa2)

	err := repo.validate(t)
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:mysql:create:validate")
	}

	res, err := mysql.BatchUpsert(repo.db, "oauth2", []interface{}{t}, repo.fieldInfo)
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:mysql:create:batch_insert")
	}

	id, err := res.LastInsertId()
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:mysql:create:last_insert_id")
	}

	// only needed in upsert
	if t.ID == 0 {
		t.ID = uint64(id)
	}

	return t.toOauth2(), nil
}

// Get an oauth2.
func (repo *Mysql) Get(ctx context.Context, id uint64) (*Oauth2, error) {
	var t []mysqlAttrib

	if err := repo.db.Select(&t, MysqlGetQuery, id); err != nil {
		return nil, errors.Wrap(err, "oauth2:mysql:get:select")
	}

	if len(t) == 0 {
		return nil, nil
	}
	return t[0].toOauth2(), nil
}

// Update an oauth2.
func (repo *Mysql) Update(ctx context.Context, oa2 *Oauth2) error {
	mysqlAttrib := toMysqlAttrib(oa2)

	if err := repo.update(ctx, mysqlAttrib); err != nil {
		return errors.Wrap(err, "oauth2:mysql:update")
	}

	return nil
}

//
func (repo *Mysql) update(ctx context.Context, p *mysqlAttrib, args ...string) error {
	var condition string
	{
		if len(args) == 0 {
			condition = fmt.Sprintf("id = %d", p.ID)
		} else {
			condition = args[0]
		}
	}

	var setStatement string
	{
		s, err := sql.PartialUpdateStmt(p, "oauth2", condition, repo.fieldInfo)
		if err != nil {
			return err
		}
		setStatement = s
	}

	_, err := repo.db.Exec(setStatement)
	return err
}

func (t *mysqlAttrib) toOauth2() *Oauth2 {
	e := &Oauth2{}
	if t == nil {
		return e
	}

	e.Id = t.ID
	e.UserId = t.UserID
	e.Provider = t.Provider
	e.AccessToken = t.AccessToken
	e.RefreshToken = t.RefreshToken
	e.TokenType = t.TokenType
	e.State = t.State
	e.Expiry, _ = timeutils.ToProto(t.Expiry)
	e.CreatedAt, _ = timeutils.ToProto(t.CreatedAt)
	e.UpdatedAt, _ = timeutils.ToProto(t.UpdatedAt)
	e.LastRefreshedAt, _ = timeutils.ToProto(t.LastRefreshedAt)

	return e
}

// toMysqlAttrib entity gateway
func toMysqlAttrib(t *Oauth2) *mysqlAttrib {
	e := &mysqlAttrib{}
	if t == nil {
		return e
	}

	e.ID = t.Id
	e.UserID = t.UserId
	e.Provider = t.Provider
	e.AccessToken = t.AccessToken
	e.RefreshToken = t.RefreshToken
	e.TokenType = t.TokenType
	e.State = t.State
	e.Expiry, _ = timeutils.FromProto(t.Expiry)
	e.LastRefreshedAt, _ = timeutils.FromProto(t.LastRefreshedAt)

	return e
}

func (repo *Mysql) validate(p *mysqlAttrib, validateOnlyPresent ...bool) error {
	_, err := repo.fieldInfo.Validate(p, validateOnlyPresent...)

	return err
}
