package oauth2

import (
	"context"
	"time"

	"github.com/alokic/gopkg/timeutils"
	"github.com/alokic/spanner/pkg/user"
	"golang.org/x/oauth2"
)

// Repository interface
type Repository interface {
	Upsert(context.Context, *Oauth2) (*Oauth2, error)
	Get(context.Context, uint64) (*Oauth2, error)
	Update(ctx context.Context, usr *Oauth2) error
}

// UserRepository interface
type UserRepository interface {
	Create(context.Context, *user.User) (*user.User, error)
	GetByEmail(context.Context, string) (*user.User, error)
	Get(context.Context, uint64) (*user.User, error)
	Update(context.Context, *user.User) error
}

// Service provides interface for oauth2 service.
type Service ServiceServer

var (
	// StateActive means oauth2 is active and not revoked.
	StateActive = "active"
	// StateRevoked means oauth2 is revoked.
	StateRevoked = "revoked"
)

// New is constructor to create Oauth2 object
func New(token *oauth2.Token, provider string, userID uint64) *Oauth2 {
	exp, _ := timeutils.ToProto(token.Expiry)
	lastRefreshed, _ := timeutils.ToProto(time.Now())

	return &Oauth2{
		UserId: userID, Provider: provider,
		AccessToken: token.AccessToken, RefreshToken: token.RefreshToken, TokenType: token.TokenType,
		State:  StateActive,
		Expiry: exp, LastRefreshedAt: lastRefreshed,
	}
}
