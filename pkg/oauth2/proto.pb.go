// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: proto.proto

package oauth2

import (
	context "context"
	fmt "fmt"
	_ "github.com/gogo/protobuf/gogoproto"
	proto "github.com/gogo/protobuf/proto"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	grpc "google.golang.org/grpc"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion2 // please upgrade the proto package

type Oauth2 struct {
	Id                   uint64               `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	UserId               uint64               `protobuf:"varint,2,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	Provider             string               `protobuf:"bytes,3,opt,name=provider,proto3" json:"provider,omitempty"`
	AccessToken          string               `protobuf:"bytes,4,opt,name=access_token,json=accessToken,proto3" json:"access_token,omitempty"`
	RefreshToken         string               `protobuf:"bytes,5,opt,name=refresh_token,json=refreshToken,proto3" json:"refresh_token,omitempty"`
	TokenType            string               `protobuf:"bytes,6,opt,name=token_type,json=tokenType,proto3" json:"token_type,omitempty"`
	State                string               `protobuf:"bytes,7,opt,name=state,proto3" json:"state,omitempty"`
	Expiry               *timestamp.Timestamp `protobuf:"bytes,8,opt,name=expiry,proto3" json:"expiry,omitempty"`
	CreatedAt            *timestamp.Timestamp `protobuf:"bytes,9,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt            *timestamp.Timestamp `protobuf:"bytes,10,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
	LastRefreshedAt      *timestamp.Timestamp `protobuf:"bytes,11,opt,name=last_refreshed_at,json=lastRefreshedAt,proto3" json:"last_refreshed_at,omitempty"`
	XXX_NoUnkeyedLiteral struct{}             `json:"-"`
	XXX_unrecognized     []byte               `json:"-"`
	XXX_sizecache        int32                `json:"-"`
}

func (m *Oauth2) Reset()         { *m = Oauth2{} }
func (m *Oauth2) String() string { return proto.CompactTextString(m) }
func (*Oauth2) ProtoMessage()    {}
func (*Oauth2) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{0}
}
func (m *Oauth2) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Oauth2.Unmarshal(m, b)
}
func (m *Oauth2) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Oauth2.Marshal(b, m, deterministic)
}
func (m *Oauth2) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Oauth2.Merge(m, src)
}
func (m *Oauth2) XXX_Size() int {
	return xxx_messageInfo_Oauth2.Size(m)
}
func (m *Oauth2) XXX_DiscardUnknown() {
	xxx_messageInfo_Oauth2.DiscardUnknown(m)
}

var xxx_messageInfo_Oauth2 proto.InternalMessageInfo

func (m *Oauth2) GetId() uint64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *Oauth2) GetUserId() uint64 {
	if m != nil {
		return m.UserId
	}
	return 0
}

func (m *Oauth2) GetProvider() string {
	if m != nil {
		return m.Provider
	}
	return ""
}

func (m *Oauth2) GetAccessToken() string {
	if m != nil {
		return m.AccessToken
	}
	return ""
}

func (m *Oauth2) GetRefreshToken() string {
	if m != nil {
		return m.RefreshToken
	}
	return ""
}

func (m *Oauth2) GetTokenType() string {
	if m != nil {
		return m.TokenType
	}
	return ""
}

func (m *Oauth2) GetState() string {
	if m != nil {
		return m.State
	}
	return ""
}

func (m *Oauth2) GetExpiry() *timestamp.Timestamp {
	if m != nil {
		return m.Expiry
	}
	return nil
}

func (m *Oauth2) GetCreatedAt() *timestamp.Timestamp {
	if m != nil {
		return m.CreatedAt
	}
	return nil
}

func (m *Oauth2) GetUpdatedAt() *timestamp.Timestamp {
	if m != nil {
		return m.UpdatedAt
	}
	return nil
}

func (m *Oauth2) GetLastRefreshedAt() *timestamp.Timestamp {
	if m != nil {
		return m.LastRefreshedAt
	}
	return nil
}

type AuthCodeURLRequest struct {
	// state_info
	StateInfo            map[string]string `protobuf:"bytes,1,rep,name=state_info,json=stateInfo,proto3" json:"state_info,omitempty" protobuf_key:"bytes,1,opt,name=key,proto3" protobuf_val:"bytes,2,opt,name=value,proto3"`
	XXX_NoUnkeyedLiteral struct{}          `json:"-"`
	XXX_unrecognized     []byte            `json:"-"`
	XXX_sizecache        int32             `json:"-"`
}

func (m *AuthCodeURLRequest) Reset()         { *m = AuthCodeURLRequest{} }
func (m *AuthCodeURLRequest) String() string { return proto.CompactTextString(m) }
func (*AuthCodeURLRequest) ProtoMessage()    {}
func (*AuthCodeURLRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{1}
}
func (m *AuthCodeURLRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AuthCodeURLRequest.Unmarshal(m, b)
}
func (m *AuthCodeURLRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AuthCodeURLRequest.Marshal(b, m, deterministic)
}
func (m *AuthCodeURLRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AuthCodeURLRequest.Merge(m, src)
}
func (m *AuthCodeURLRequest) XXX_Size() int {
	return xxx_messageInfo_AuthCodeURLRequest.Size(m)
}
func (m *AuthCodeURLRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_AuthCodeURLRequest.DiscardUnknown(m)
}

var xxx_messageInfo_AuthCodeURLRequest proto.InternalMessageInfo

func (m *AuthCodeURLRequest) GetStateInfo() map[string]string {
	if m != nil {
		return m.StateInfo
	}
	return nil
}

type AuthCodeURLResponse struct {
	Url                  string   `protobuf:"bytes,1,opt,name=url,proto3" json:"url,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *AuthCodeURLResponse) Reset()         { *m = AuthCodeURLResponse{} }
func (m *AuthCodeURLResponse) String() string { return proto.CompactTextString(m) }
func (*AuthCodeURLResponse) ProtoMessage()    {}
func (*AuthCodeURLResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{2}
}
func (m *AuthCodeURLResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_AuthCodeURLResponse.Unmarshal(m, b)
}
func (m *AuthCodeURLResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_AuthCodeURLResponse.Marshal(b, m, deterministic)
}
func (m *AuthCodeURLResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_AuthCodeURLResponse.Merge(m, src)
}
func (m *AuthCodeURLResponse) XXX_Size() int {
	return xxx_messageInfo_AuthCodeURLResponse.Size(m)
}
func (m *AuthCodeURLResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_AuthCodeURLResponse.DiscardUnknown(m)
}

var xxx_messageInfo_AuthCodeURLResponse proto.InternalMessageInfo

func (m *AuthCodeURLResponse) GetUrl() string {
	if m != nil {
		return m.Url
	}
	return ""
}

type SignUpRequest struct {
	// state received in callback from oauth2 provider
	State string `protobuf:"bytes,1,opt,name=state,proto3" json:"state,omitempty"`
	// code received in callback from oauth2 provider
	Code                 string   `protobuf:"bytes,2,opt,name=code,proto3" json:"code,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SignUpRequest) Reset()         { *m = SignUpRequest{} }
func (m *SignUpRequest) String() string { return proto.CompactTextString(m) }
func (*SignUpRequest) ProtoMessage()    {}
func (*SignUpRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{3}
}
func (m *SignUpRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SignUpRequest.Unmarshal(m, b)
}
func (m *SignUpRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SignUpRequest.Marshal(b, m, deterministic)
}
func (m *SignUpRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SignUpRequest.Merge(m, src)
}
func (m *SignUpRequest) XXX_Size() int {
	return xxx_messageInfo_SignUpRequest.Size(m)
}
func (m *SignUpRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_SignUpRequest.DiscardUnknown(m)
}

var xxx_messageInfo_SignUpRequest proto.InternalMessageInfo

func (m *SignUpRequest) GetState() string {
	if m != nil {
		return m.State
	}
	return ""
}

func (m *SignUpRequest) GetCode() string {
	if m != nil {
		return m.Code
	}
	return ""
}

type SignUpResponse struct {
	UserId uint64 `protobuf:"varint,1,opt,name=user_id,json=userId,proto3" json:"user_id,omitempty"`
	// token is jwt token
	Token                string   `protobuf:"bytes,2,opt,name=token,proto3" json:"token,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SignUpResponse) Reset()         { *m = SignUpResponse{} }
func (m *SignUpResponse) String() string { return proto.CompactTextString(m) }
func (*SignUpResponse) ProtoMessage()    {}
func (*SignUpResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{4}
}
func (m *SignUpResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SignUpResponse.Unmarshal(m, b)
}
func (m *SignUpResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SignUpResponse.Marshal(b, m, deterministic)
}
func (m *SignUpResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SignUpResponse.Merge(m, src)
}
func (m *SignUpResponse) XXX_Size() int {
	return xxx_messageInfo_SignUpResponse.Size(m)
}
func (m *SignUpResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_SignUpResponse.DiscardUnknown(m)
}

var xxx_messageInfo_SignUpResponse proto.InternalMessageInfo

func (m *SignUpResponse) GetUserId() uint64 {
	if m != nil {
		return m.UserId
	}
	return 0
}

func (m *SignUpResponse) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

type SignInRequest struct {
	// token is jwt token
	Token                string   `protobuf:"bytes,1,opt,name=token,proto3" json:"token,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SignInRequest) Reset()         { *m = SignInRequest{} }
func (m *SignInRequest) String() string { return proto.CompactTextString(m) }
func (*SignInRequest) ProtoMessage()    {}
func (*SignInRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{5}
}
func (m *SignInRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SignInRequest.Unmarshal(m, b)
}
func (m *SignInRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SignInRequest.Marshal(b, m, deterministic)
}
func (m *SignInRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SignInRequest.Merge(m, src)
}
func (m *SignInRequest) XXX_Size() int {
	return xxx_messageInfo_SignInRequest.Size(m)
}
func (m *SignInRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_SignInRequest.DiscardUnknown(m)
}

var xxx_messageInfo_SignInRequest proto.InternalMessageInfo

func (m *SignInRequest) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

type SignInResponse struct {
	// email of authenticating user
	Email                string   `protobuf:"bytes,1,opt,name=email,proto3" json:"email,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *SignInResponse) Reset()         { *m = SignInResponse{} }
func (m *SignInResponse) String() string { return proto.CompactTextString(m) }
func (*SignInResponse) ProtoMessage()    {}
func (*SignInResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_2fcc84b9998d60d8, []int{6}
}
func (m *SignInResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SignInResponse.Unmarshal(m, b)
}
func (m *SignInResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SignInResponse.Marshal(b, m, deterministic)
}
func (m *SignInResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SignInResponse.Merge(m, src)
}
func (m *SignInResponse) XXX_Size() int {
	return xxx_messageInfo_SignInResponse.Size(m)
}
func (m *SignInResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_SignInResponse.DiscardUnknown(m)
}

var xxx_messageInfo_SignInResponse proto.InternalMessageInfo

func (m *SignInResponse) GetEmail() string {
	if m != nil {
		return m.Email
	}
	return ""
}

func init() {
	proto.RegisterType((*Oauth2)(nil), "oauth2.Oauth2")
	proto.RegisterType((*AuthCodeURLRequest)(nil), "oauth2.AuthCodeURLRequest")
	proto.RegisterMapType((map[string]string)(nil), "oauth2.AuthCodeURLRequest.StateInfoEntry")
	proto.RegisterType((*AuthCodeURLResponse)(nil), "oauth2.AuthCodeURLResponse")
	proto.RegisterType((*SignUpRequest)(nil), "oauth2.SignUpRequest")
	proto.RegisterType((*SignUpResponse)(nil), "oauth2.SignUpResponse")
	proto.RegisterType((*SignInRequest)(nil), "oauth2.SignInRequest")
	proto.RegisterType((*SignInResponse)(nil), "oauth2.SignInResponse")
}

func init() { proto.RegisterFile("proto.proto", fileDescriptor_2fcc84b9998d60d8) }

var fileDescriptor_2fcc84b9998d60d8 = []byte{
	// 550 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x84, 0x52, 0x5d, 0x8b, 0xd3, 0x40,
	0x14, 0x25, 0xfd, 0x48, 0x37, 0x37, 0xbb, 0x55, 0xc7, 0xaa, 0x21, 0x22, 0xd6, 0x88, 0x5a, 0x1f,
	0xcc, 0x42, 0x7d, 0xd0, 0x15, 0x41, 0x8a, 0xb8, 0x58, 0x10, 0x84, 0xb4, 0xfb, 0x1c, 0xd2, 0xe4,
	0x36, 0x0d, 0xdb, 0x66, 0xe2, 0xcc, 0xa4, 0xd8, 0xff, 0xe2, 0x4f, 0xf1, 0xcd, 0x3f, 0x26, 0x99,
	0xc9, 0x74, 0x1b, 0x3f, 0xe8, 0x4b, 0x99, 0x73, 0xef, 0x39, 0xf7, 0xf6, 0xe4, 0x1e, 0xb0, 0x0b,
	0x46, 0x05, 0xf5, 0xe5, 0x2f, 0x31, 0x69, 0x54, 0x8a, 0xd5, 0xd8, 0x7d, 0x95, 0x66, 0x62, 0x55,
	0x2e, 0xfc, 0x98, 0x6e, 0xce, 0x53, 0x9a, 0xd2, 0x73, 0xd9, 0x5e, 0x94, 0x4b, 0x89, 0x24, 0x90,
	0x2f, 0x25, 0x73, 0x1f, 0xa7, 0x94, 0xa6, 0x6b, 0xbc, 0x61, 0x89, 0x6c, 0x83, 0x5c, 0x44, 0x9b,
	0x42, 0x11, 0xbc, 0x9f, 0x6d, 0x30, 0xbf, 0xca, 0xd1, 0xa4, 0x0f, 0xad, 0x2c, 0x71, 0x8c, 0xa1,
	0x31, 0xea, 0x04, 0xad, 0x2c, 0x21, 0x0f, 0xa0, 0x57, 0x72, 0x64, 0x61, 0x96, 0x38, 0x2d, 0x59,
	0x34, 0x2b, 0x38, 0x4d, 0x88, 0x0b, 0x27, 0x05, 0xa3, 0xdb, 0x2c, 0x41, 0xe6, 0xb4, 0x87, 0xc6,
	0xc8, 0x0a, 0xf6, 0x98, 0x3c, 0x81, 0xd3, 0x28, 0x8e, 0x91, 0xf3, 0x50, 0xd0, 0x6b, 0xcc, 0x9d,
	0x8e, 0xec, 0xdb, 0xaa, 0x36, 0xaf, 0x4a, 0xe4, 0x29, 0x9c, 0x31, 0x5c, 0x32, 0xe4, 0xab, 0x9a,
	0xd3, 0x95, 0x9c, 0xd3, 0xba, 0xa8, 0x48, 0x8f, 0x00, 0x64, 0x33, 0x14, 0xbb, 0x02, 0x1d, 0x53,
	0x32, 0x2c, 0x59, 0x99, 0xef, 0x0a, 0x24, 0x03, 0xe8, 0x72, 0x11, 0x09, 0x74, 0x7a, 0xb2, 0xa3,
	0x00, 0x19, 0x83, 0x89, 0xdf, 0x8b, 0x8c, 0xed, 0x9c, 0x93, 0xa1, 0x31, 0xb2, 0xc7, 0xae, 0xaf,
	0xec, 0xfb, 0xda, 0xbe, 0x3f, 0xd7, 0xf6, 0x83, 0x9a, 0x49, 0x2e, 0x00, 0x62, 0x86, 0x91, 0xc0,
	0x24, 0x8c, 0x84, 0x63, 0x1d, 0xd5, 0x59, 0x35, 0x7b, 0x22, 0x2a, 0x69, 0x59, 0x24, 0x5a, 0x0a,
	0xc7, 0xa5, 0x35, 0x7b, 0x22, 0xc8, 0x25, 0xdc, 0x59, 0x47, 0x5c, 0x84, 0xb5, 0x67, 0x35, 0xc1,
	0x3e, 0x3a, 0xe1, 0x56, 0x25, 0x0a, 0xb4, 0x66, 0x22, 0xbc, 0x1f, 0x06, 0x90, 0x49, 0x29, 0x56,
	0x1f, 0x69, 0x82, 0x57, 0xc1, 0x97, 0x00, 0xbf, 0x95, 0xc8, 0x05, 0xf9, 0x0c, 0x20, 0xbf, 0x48,
	0x98, 0xe5, 0x4b, 0xea, 0x18, 0xc3, 0xf6, 0xc8, 0x1e, 0xbf, 0xf4, 0x55, 0x84, 0xfc, 0xbf, 0xf9,
	0xfe, 0xac, 0x22, 0x4f, 0xf3, 0x25, 0xfd, 0x94, 0x0b, 0xb6, 0x0b, 0x2c, 0xae, 0xb1, 0xfb, 0x1e,
	0xfa, 0xcd, 0x26, 0xb9, 0x0d, 0xed, 0x6b, 0xdc, 0xc9, 0x9c, 0x58, 0x41, 0xf5, 0xac, 0x8e, 0xb1,
	0x8d, 0xd6, 0x25, 0xca, 0x98, 0x58, 0x81, 0x02, 0xef, 0x5a, 0x6f, 0x0d, 0xef, 0x05, 0xdc, 0x6d,
	0x6c, 0xe3, 0x05, 0xcd, 0x39, 0x56, 0x23, 0x4a, 0xb6, 0xd6, 0x23, 0x4a, 0xb6, 0xf6, 0x2e, 0xe0,
	0x6c, 0x96, 0xa5, 0xf9, 0x55, 0xa1, 0x1d, 0xec, 0x0f, 0x6c, 0x1c, 0x1e, 0x98, 0x40, 0x27, 0xa6,
	0x89, 0x5e, 0x24, 0xdf, 0xde, 0x07, 0xe8, 0x6b, 0x69, 0x3d, 0xfe, 0x20, 0xb8, 0x46, 0x23, 0xb8,
	0x03, 0xe8, 0xaa, 0xc4, 0xd5, 0x7f, 0x54, 0x02, 0xef, 0x99, 0xda, 0x3d, 0xcd, 0x0f, 0x76, 0x2b,
	0x9a, 0x71, 0x48, 0x7b, 0xae, 0xf6, 0x54, 0xb4, 0x7a, 0xcf, 0x00, 0xba, 0xb8, 0x89, 0x32, 0x6d,
	0x44, 0x81, 0xf1, 0x2f, 0x03, 0x7a, 0x33, 0x64, 0xdb, 0x2c, 0x46, 0x72, 0x09, 0xf6, 0x81, 0x7f,
	0xe2, 0xfe, 0xff, 0x04, 0xee, 0xc3, 0x7f, 0xf6, 0xea, 0x4d, 0x6f, 0xc0, 0x54, 0x1e, 0xc9, 0x3d,
	0x4d, 0x6b, 0x7c, 0x2e, 0xf7, 0xfe, 0x9f, 0xe5, 0xa6, 0x70, 0x9a, 0x37, 0x85, 0x7b, 0xaf, 0x4d,
	0xe1, 0x8d, 0xb7, 0x85, 0x29, 0xd3, 0xf7, 0xfa, 0x77, 0x00, 0x00, 0x00, 0xff, 0xff, 0x4e, 0x09,
	0x43, 0x1d, 0x85, 0x04, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// ServiceClient is the client API for Service service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type ServiceClient interface {
	AuthCodeURL(ctx context.Context, in *AuthCodeURLRequest, opts ...grpc.CallOption) (*AuthCodeURLResponse, error)
	SignUp(ctx context.Context, in *SignUpRequest, opts ...grpc.CallOption) (*SignUpResponse, error)
	SignIn(ctx context.Context, in *SignInRequest, opts ...grpc.CallOption) (*SignInResponse, error)
}

type serviceClient struct {
	cc *grpc.ClientConn
}

func NewServiceClient(cc *grpc.ClientConn) ServiceClient {
	return &serviceClient{cc}
}

func (c *serviceClient) AuthCodeURL(ctx context.Context, in *AuthCodeURLRequest, opts ...grpc.CallOption) (*AuthCodeURLResponse, error) {
	out := new(AuthCodeURLResponse)
	err := c.cc.Invoke(ctx, "/oauth2.Service/AuthCodeURL", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *serviceClient) SignUp(ctx context.Context, in *SignUpRequest, opts ...grpc.CallOption) (*SignUpResponse, error) {
	out := new(SignUpResponse)
	err := c.cc.Invoke(ctx, "/oauth2.Service/SignUp", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *serviceClient) SignIn(ctx context.Context, in *SignInRequest, opts ...grpc.CallOption) (*SignInResponse, error) {
	out := new(SignInResponse)
	err := c.cc.Invoke(ctx, "/oauth2.Service/SignIn", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ServiceServer is the server API for Service service.
type ServiceServer interface {
	AuthCodeURL(context.Context, *AuthCodeURLRequest) (*AuthCodeURLResponse, error)
	SignUp(context.Context, *SignUpRequest) (*SignUpResponse, error)
	SignIn(context.Context, *SignInRequest) (*SignInResponse, error)
}

func RegisterServiceServer(s *grpc.Server, srv ServiceServer) {
	s.RegisterService(&_Service_serviceDesc, srv)
}

func _Service_AuthCodeURL_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AuthCodeURLRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ServiceServer).AuthCodeURL(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/oauth2.Service/AuthCodeURL",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ServiceServer).AuthCodeURL(ctx, req.(*AuthCodeURLRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Service_SignUp_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SignUpRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ServiceServer).SignUp(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/oauth2.Service/SignUp",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ServiceServer).SignUp(ctx, req.(*SignUpRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Service_SignIn_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SignInRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ServiceServer).SignIn(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/oauth2.Service/SignIn",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ServiceServer).SignIn(ctx, req.(*SignInRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _Service_serviceDesc = grpc.ServiceDesc{
	ServiceName: "oauth2.Service",
	HandlerType: (*ServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "AuthCodeURL",
			Handler:    _Service_AuthCodeURL_Handler,
		},
		{
			MethodName: "SignUp",
			Handler:    _Service_SignUp_Handler,
		},
		{
			MethodName: "SignIn",
			Handler:    _Service_SignIn_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "proto.proto",
}
