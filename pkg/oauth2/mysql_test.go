package oauth2_test

import (
	"context"
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/alokic/gopkg/timeutils"
	"github.com/alokic/gopkg/typeutils"
	"github.com/alokic/spanner/pkg/mysql"
	"github.com/alokic/spanner/pkg/oauth2"
	"github.com/alokic/spanner/pkg/testhelper"
)

type testOauth2Conf struct {
	db *testhelper.Tx
}

type testComparator func(interface{}, interface{}) bool

func TestMysql_Upsert(t *testing.T) {
	conf, teardown := testSetup()
	defer teardown()

	type args struct {
		ctx context.Context
		oth *oauth2.Oauth2
	}
	tests := []struct {
		name         string
		args         args
		want         *oauth2.Oauth2
		wantErr      bool
		wantErrValue error
	}{
		{
			name:         "TestSuccess",
			args:         args{ctx: context.Background(), oth: testNewOauth2(1, 1)},
			want:         testNewOauth2(1, 1),
			wantErr:      false,
			wantErrValue: nil,
		},
		{
			name:         "TestDuplicateIDUserIDProvider",
			args:         args{ctx: context.Background(), oth: testNewOauth2(2, 2)},
			want:         testNewOauth2(2, 2),
			wantErr:      false,
			wantErrValue: nil,
		},
		{
			name:         "TestDuplicateUserIDProvider",
			args:         args{ctx: context.Background(), oth: testNewOauth2(0, 2)},
			want:         testNewOauth2(2, 2),
			wantErr:      false,
			wantErrValue: nil,
		},
	}

	testUpsertOauth2(conf.db, testNewOauth2(2, 2))

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := oauth2.NewMysql(conf.db)

			got, err := repo.Upsert(tt.args.ctx, tt.args.oth)
			if (err != nil) != tt.wantErr {
				t.Errorf("Mysql.Upsert() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if err != nil && !testCompare(err, tt.wantErrValue, func(e1, e2 interface{}) bool {
				return strings.Contains(e1.(error).Error(), e2.(error).Error())
			}) {
				t.Errorf("Mysql.Upsert() errorValue = %v, wantErrValue %v", err, tt.wantErrValue)
				return
			}

			if !testCompare(got, tt.want, func(e1, e2 interface{}) bool {
				u1, _ := e1.(*oauth2.Oauth2)
				u2, _ := e2.(*oauth2.Oauth2)
				return u1.Id == u2.Id && u1.UserId == u2.UserId && u1.Provider == u2.Provider
			}) {
				t.Errorf("Mysql.Upsert() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMysql_Get(t *testing.T) {
	conf, teardown := testSetup()
	defer teardown()

	u1, _ := testUpsertOauth2(conf.db, testNewOauth2(1, 1))

	type args struct {
		ctx context.Context
		id  uint64
	}
	tests := []struct {
		name         string
		args         args
		want         *oauth2.Oauth2
		wantErr      bool
		wantErrValue error
	}{
		{
			name:         "TestSuccess",
			args:         args{ctx: context.Background(), id: u1.Id},
			want:         u1,
			wantErr:      false,
			wantErrValue: nil,
		},
		{
			name:         "TestNonExistent",
			args:         args{ctx: context.Background(), id: 1000},
			want:         nil,
			wantErr:      false,
			wantErrValue: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := oauth2.NewMysql(conf.db)

			got, err := repo.Get(tt.args.ctx, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Mysql.Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if err != nil && !testCompare(err, tt.wantErrValue, func(e1, e2 interface{}) bool {
				return strings.Contains(e1.(error).Error(), e2.(error).Error())
			}) {
				t.Errorf("Mysql.Get() errorValue = %v, wantErrValue %v", err, tt.wantErrValue)
				return
			}

			if !testCompare(got, tt.want, testCompareOauth2) {
				t.Errorf("Mysql.Get() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func testCompare(e1, e2 interface{}, fn testComparator) bool {
	if typeutils.Blank(e1) && typeutils.Blank(e2) {
		return true
	}

	if typeutils.Blank(e1) || typeutils.Blank(e2) {
		return false
	}

	return fn(e1, e2)
}

func testCompareOauth2(e1, e2 interface{}) bool {
	u1, _ := e1.(*oauth2.Oauth2)
	u2, _ := e2.(*oauth2.Oauth2)
	return u1.Id == u2.Id && u1.UserId == u2.UserId && u1.Provider == u2.Provider
}

func testNewOauth2(id, userId uint64) *oauth2.Oauth2 {
	exp, _ := timeutils.ToProto(time.Now().AddDate(0, 0, int(id)))
	lastRefreshedAt, _ := timeutils.ToProto(time.Now())
	return &oauth2.Oauth2{
		Id: id, UserId: userId,
		AccessToken: fmt.Sprintf("access_token_%d", id), RefreshToken: fmt.Sprintf("refresh_token_%d", id),
		Provider: "google", TokenType: "Bearer", Expiry: exp, State: oauth2.StateActive, LastRefreshedAt: lastRefreshedAt,
	}
}

func testUpsertOauth2(db mysql.DB, p *oauth2.Oauth2) (*oauth2.Oauth2, error) {
	repo := oauth2.NewMysql(db)
	return repo.Upsert(context.Background(), p)
}

func testSetup() (*testOauth2Conf, func()) {
	db := testhelper.GetMysql()
	db.SeedDB()
	return &testOauth2Conf{db: db}, func() {
		db.CleanDB()
	}
}
