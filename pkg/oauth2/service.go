package oauth2

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/alokic/gopkg/timeutils"
	"github.com/alokic/gopkg/typeutils"
	"github.com/alokic/spanner/pkg/user"
	"github.com/pkg/errors"
	"golang.org/x/oauth2"
)

// JWTer interface
type JWTer interface {
	VerifyToken(string) (map[string]interface{}, error)
	Generate(map[string]interface{}) (string, error)
}

// Input struct.
type Input struct {
	Config              *oauth2.Config
	Provider            string
	UserInfoURL         string
	TokenVerifyInterval time.Duration
	JWT                 JWTer
	Oauth2Repo          Repository
	UserRepo            UserRepository
}

type service struct {
	*Input
}

var (
	// ErrCallbackEmptyState means no state is received in callback.
	ErrCallbackEmptyState = errors.New("state not received in  oauth2 callback")
	// ErrCallbackInavlidState means invalid state is received in callback.
	ErrCallbackInavlidState = errors.New("invalid state received in  oauth2 callback")
	// ErrCallbackEmptyCode means authorization code is not received in callback.
	ErrCallbackEmptyCode = errors.New("authorization code not received in  oauth2 callback")
	// ErrInvalidToken means invalid token is provided in sign_in.
	ErrInvalidToken = errors.New("invalid token in sign_in")
	// ErrUserBlocked means sign_in failed as user is blocked.
	ErrUserBlocked = errors.New("sign_in failed as user is blocked")
	// ErrUserNotFound means user not found.
	ErrUserNotFound = errors.New("user not found")
)

var ()

// NewService is constructor
func NewService(in *Input) Service {
	return &service{
		Input: in,
	}
}

// AuthCodeURL returns authrization code url.
func (s *service) AuthCodeURL(ctx context.Context, in *AuthCodeURLRequest) (*AuthCodeURLResponse, error) {
	m := map[string]interface{}{}
	for k, v := range in.StateInfo {
		m[k] = v
	}

	state, err := s.JWT.Generate(m)
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:service:auth_code_url")
	}

	return &AuthCodeURLResponse{Url: s.Config.AuthCodeURL(state, oauth2.AccessTypeOffline)}, nil
}

// SignUp is called from callback handler.
func (s *service) SignUp(ctx context.Context, in *SignUpRequest) (*SignUpResponse, error) {
	if in.State == "" {
		return nil, errors.Wrap(ErrCallbackEmptyState, "oauth2:service:sign_up")
	}

	if in.Code == "" {
		return nil, errors.Wrap(ErrCallbackEmptyCode, "oauth2:service:sign_up")
	}

	_, err := s.JWT.VerifyToken(in.State)
	if err != nil {
		return nil, errors.Wrap(ErrCallbackInavlidState, "oauth2:service:sign_up")
	}

	token, err := s.Config.Exchange(ctx, in.Code, oauth2.AccessTypeOffline)
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:service:sign_up")
	}

	u, err := s.createUser(ctx, token)
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:service:sign_up")
	}

	o, err := s.upsertToken(ctx, token, u.Id)
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:service:sign_up")
	}

	jwttoken, err := s.JWT.Generate(map[string]interface{}{"token_id": o.Id, "user_id": o.UserId, "ts": time.Now()})
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:service:sign_up")
	}

	return &SignUpResponse{UserId: o.UserId, Token: jwttoken}, nil
}

// SignIn is called during regular authentication of api.
func (s *service) SignIn(ctx context.Context, in *SignInRequest) (*SignInResponse, error) {
	claims, err := s.JWT.VerifyToken(in.Token)
	if err != nil {
		return nil, errors.Wrap(ErrInvalidToken, "oauth2:service:sign_in")
	}

	u, err := s.UserRepo.Get(ctx, typeutils.ToId(claims["user_id"]))
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:service:sign_in")
	}

	if u == nil {
		return nil, errors.Wrap(ErrUserNotFound, "oauth2:service:sign_in")
	}

	if u.State == user.StateBlocked {
		return nil, errors.Wrap(ErrUserBlocked, "oauth2:service:sign_in")
	}

	oa2, err := s.Oauth2Repo.Get(ctx, typeutils.ToId(claims["token_id"]))
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:service:sign_in")
	}

	err = s.verifyToken(ctx, u, oa2)
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:service:sign_in")
	}

	return &SignInResponse{Email: typeutils.ToStr(u.Email)}, nil
}

func (s *service) createUser(ctx context.Context, token *oauth2.Token) (*user.User, error) {
	uinfo, err := s.fetchUserInfo(ctx, token)
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:service:create_user")
	}

	u, err := s.UserRepo.GetByEmail(ctx, uinfo.Email)
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:service:create_user")
	}

	if u != nil {
		return u, nil
	}

	u, err = user.New(uinfo.Name, uinfo.Email)
	if err != nil {
		return nil, errors.Wrap(err, "oauth2:service:create_user")
	}

	return s.UserRepo.Create(ctx, u)
}

func (s *service) verifyToken(ctx context.Context, usr *user.User, oa2 *Oauth2) error {
	if !s.shouldVerifyToken(oa2) {
		return nil
	}

	newtoken, err := s.validateTokenFromRemote(ctx, oa2)
	if err != nil {
		s.updateToken(ctx, &Oauth2{Id: oa2.Id, State: StateRevoked})
		return errors.Wrap(err, "oauth2:service:verify_token")
	}

	// return if both tokens are same
	if newtoken.AccessToken == oa2.AccessToken {
		return nil
	}

	_, err = s.upsertToken(ctx, newtoken, usr.Id)
	return err
}

func (s *service) upsertToken(ctx context.Context, token *oauth2.Token, userID uint64) (*Oauth2, error) {
	return s.Oauth2Repo.Upsert(ctx, New(token, s.Provider, userID))
}

func (s *service) updateToken(ctx context.Context, oa2 *Oauth2) error {
	return s.Oauth2Repo.Update(ctx, oa2)
}

func (s *service) validateTokenFromRemote(ctx context.Context, oa2 *Oauth2) (*oauth2.Token, error) {
	exp, err := timeutils.FromProto(oa2.Expiry)
	if err != nil {
		return nil, errors.Wrap(err, "oauth2.service:verify")
	}

	newtoken := &oauth2.Token{
		AccessToken:  oa2.AccessToken,
		TokenType:    oa2.TokenType,
		RefreshToken: oa2.RefreshToken,
		Expiry:       exp,
	}

	_, err = s.fetchUserInfo(ctx, newtoken)
	if err != nil {
		return nil, errors.Wrap(err, "oauth2.service:verify")
	}

	return newtoken, nil

}

// UserInfo struct.
type UserInfo struct {
	Sub           string `json:"sub"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Profile       string `json:"profile"`
	Picture       string `json:"picture"`
	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
	Gender        string `json:"gender"`
}

// fetch userinfo from provider.
func (s *service) fetchUserInfo(ctx context.Context, token *oauth2.Token) (*UserInfo, error) {
	resp, err := s.Config.Client(ctx, token).Get(s.UserInfoURL)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, errors.New(resp.Status)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var result UserInfo
	if err := json.Unmarshal(data, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

func (s *service) shouldVerifyToken(oa2 *Oauth2) bool {
	t, err := timeutils.FromProto(oa2.LastRefreshedAt)
	if err != nil {
		return true
	}
	return time.Now().Sub(t) > s.TokenVerifyInterval
}
